"""
Improve obtained model from run_experiments_own_bow.py using AL by performing
some experiments.
"""
import cPickle
import copy
from datetime import datetime
import random
import os

from sklearn.naive_bayes import MultinomialNB
from sklearn.multiclass import OneVsOneClassifier, OneVsRestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.metrics import f1_score, confusion_matrix
from sklearn.base import clone
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm

from Data_task_4.al_strategies.uncertainty_sampling import UncertaintySampling
from Data_task_4.al_strategies.certainty_sampling import CertaintySampling
from Data_task_4.experiments.run_experiments_own_bow import \
    _write_confusion_matrix
from Data_task_4.experiments.run_experiments_own_bow import _get_cv_iterator,\
    _calculate_f1


def plot_f1(scores, budgets, al_names, dst=None, title=None):
    """
    Plots the average F1 scores of the different AL strategies in a learning
    curve.

    Parameters
    ----------
    scores: List of lists where each list holds the average F1 scores that
            should be plotted.
    budgets: List of budgets that were used to measure the F1 scores.
    al_name: List of names to be displayed for the AL strategies. It must
             have the same order as <scores>.
    dst: Path where the file should be stored. None means it won't be stored.
    title: Additional information that should be added in the title of the plot.

    """
    print "scores", scores
    print len(scores)
    print "names", al_names
    print len(al_names)
    plt.clf()
    title = "" if title is None else title
    # How many colors are needed?
    n = len(al_names)
    colors = cm.rainbow(np.linspace(0, 1, n))
    # colors = ["red", "blue"]
    # F1 score per strategy. Each F1 score was measured with different budgets
    for idx, avg_f1 in enumerate(scores):
        y = avg_f1
        plt.plot(budgets, y, label="{}".format(al_names[idx]), c=colors[idx])
    plt.xlabel('Number of queries')
    plt.ylabel('Weighted average F1 score')
    plt.title('Learning curves ' + title)
    plt.legend(loc="upper center", ncol=2, bbox_to_anchor=(0.5, 1.0),
               prop={'size': 10})
    plt.ylim(-0.05, 1.05)
    # Prevent clipping of x-axis
    plt.margins(0.1)
    # plt.show()
    # Store figure
    if dst is not None:
        plt.savefig(dst, dpi=300)


def _choose_seed_set(tweets, size):
    """
    Chooses which instances to be used for the initial seed set for AL.
    Instances are picked randomly. Guarantees that all 3 classes are present
    in the seed set at least once.

    Parameters
    ----------
    tweets: List of all Tweet objects in the dataset.
    size: Number of instances to be retrieved for the seed set.

    Returns
    -------
    List, list.
    List of Tweet objects that are to be used as seed set. List of indices of
    those instances.

    """
    seed_tweets = []
    seed_idc = []
    classes = set()
    while len(seed_idc) < size:
        idx = random.randint(0, len(tweets) - 1)
        while idx in seed_idc:
            idx = random.randint(0, len(tweets) - 1)
        # Now pick one of the tweets with other labels for the last 2
        # instances if none are present yet
        if len(seed_idc) <= size - 2 and len(classes) < 3:
            idx = random.randint(0, len(tweets) - 1)
            while tweets[idx].label in classes:
                idx = random.randint(0, len(tweets) - 1)
        seed_idc.append(idx)
        seed_tweets.append(tweets[idx])
        classes.add(tweets[idx].label)
    return seed_tweets, seed_idc


def create_seed_set_and_training_set(tweets, size):
    """
    Creates the seed set and the remaining unlabeled pool based on a list of
    Tweet objects. Both sets are disjoint.

    Parameters
    ----------
    tweets: List of all Tweet objects in the dataset.
    size: Number of instances to be retrieved for the seed set.

    Returns
    -------
    List, list.
    List of Tweet objects for seed set and list of Tweet objects for
    unlabeled pool.

    """
    seed_tweets, seed_idc = _choose_seed_set(tweets, size)
    unlabeled_tweets = []
    for idx, tweet in enumerate(tweets):
        if idx not in seed_idc:
            unlabeled_tweets.append(tweet)
    return seed_tweets, unlabeled_tweets


def _choose_training_instances(tweets, size):
    """
    Chooses which instances to be used for the initial seed set for AL.
    Instances are picked randomly. Guarantees that all 3 classes are present
    in the seed set at least once.

    Parameters
    ----------
    tweets: List of all Tweet objects in the dataset.
    size: Number of instances to be retrieved for the seed set.

    Returns
    -------
    List, list.
    List of Tweet objects that are to be used as seed set. List of indices of
    those instances.

    """
    try:
        seed_tweets = random.sample(tweets, size)
    except ValueError:
        # Budget > unlabeled instances
        seed_tweets = tweets
    seed_idc = []
    for idx, tweet in enumerate(tweets):
        for t in seed_tweets:
            if tweet.tweet_id == t.tweet_id:
                seed_idc.append(idx)
                break
    # seed_tweets = []
    # seed_idc = []
    # classes = set()
    # # No test set -> any instance can be selected as training instance
    # while len(seed_idc) < size:
    #     idx = random.randint(0, len(tweets) - 1)
    #     while idx in seed_idc:
    #         idx = random.randint(0, len(tweets) - 1)
    #     seed_idc.append(idx)
    #     seed_tweets.append(tweets[idx])
    #     classes.add(tweets[idx].label)
    return seed_tweets, seed_idc


def _get_classifier_name(clf):
    """
    Returns the name of a classifier as a string.
    """
    if isinstance(clf, OneVsRestClassifier) or isinstance(
            clf, OneVsOneClassifier):
        if isinstance(clf.get_params()["estimator"], MultinomialNB):
            return "MNB"
        if isinstance(clf.get_params()["estimator"], SVC):
            return "SVM"
    elif isinstance(clf, LogisticRegression):
        return "LR"


def compute_baseline(clf_, seed, training, budgets, repetitions, test=None):
    """
    Computes a baseline, i.e., it performs supervised learning with a given
    budget. Should roughly indicate how supervised learning would perform.
    "Roughly" because we don't use different classifiers for different
    feature spaces in here.

    Parameters
    ----------
    clf: SciKit classifier object that should be used for supervised learning.
    training: List of Tweet objects to be used for training the model.
              Represents the unlabeled pool.
    seed: List of Tweet objects that are already available to the classifier.
          Corresponds to the labeled pool.
    budgets: List of budgets to be used for building learners.
    strats: List of strategies to be used. Must have the same length as
            <names>. Values are "uc" for uncertainty sampling or "c" for
            certainty sampling.
    al_types: List of types to be used. It could either be "confidence" or
              "margin". Must have the same length as <strats>.
    seed_set_size: Number of instances to be used in seed set.
    out_dir: Directory in which the results should be stored.
    test: List of Tweet objects to be used as hold-out test set. If None,
          the results for CV are plotted.
    seed: Seed value for PRNG.
    k: Number of folds in CV.

    Returns
    -------
    List.
    List of F1 scores for the respective budgets.

    """
    f1 = []
    CLASSES = ["negative", "neutral", "positive"]
    total_cm = np.zeros((len(CLASSES), len(CLASSES)))
    for budget in budgets:
        # f1_tmp = []
        cm_tmp = np.zeros((len(CLASSES), len(CLASSES)))
        # Repeat multiple times to average results - it's a kind of CV
        for rep in xrange(repetitions):
            # Choose training set arbitrarily
            clf = clone(clf_)
            training_tweets, training_idc = _choose_training_instances(
                training, budget)
            X_u = [t.bow for t in training_tweets]
            y_u = [t.label for t in training_tweets]
            # Now incorporate seed set into training data
            X_u.extend([t.bow for t in seed])
            y_u.extend([t.label for t in seed])
            X_u = np.array(X_u)
            y_u = np.array(y_u)
            print "labels in training", len(y_u)
            clf.fit(X_u, y_u)
            X_t = []
            y_t = []
            # Use remaining instances as test set assuming no separate set is
            #  specified
            if test is None:
                for idx, t in enumerate(tweets):
                    if idx not in training_idc:
                        X_t.append(t.bow)
                        y_t.append(t.label)
            else:
                X_t = np.array([t.bow for t in test])
                y_t = np.array([t.label for t in test])
            preds = clf.predict(X_t)
            cm = confusion_matrix(y_t, preds, labels=np.array(CLASSES))
            cm_tmp += cm
        total_cm += cm_tmp

        _, avg_f1_tmp = _calculate_f1(cm_tmp, np.array(CLASSES))
        f1.append(avg_f1_tmp)
        # avg_f1_score = sum(f1_tmp) / float(len(f1_tmp))
        # f1.append(avg_f1_score)
    _, avg_f1 = _calculate_f1(cm, np.array(CLASSES))
    print "Overall Average F1", avg_f1
    print "all average f1 scores for the different budgets", f1
    return f1


def compute_baseline2(clf_, seed, training, budget, test=None):
    """
    Computes a baseline, i.e., it performs supervised learning with a given
    budget. Should roughly indicate how supervised learning would perform.
    "Roughly" because we don't use different classifiers for different
    feature spaces in here.

    Parameters
    ----------
    clf_: SciKit classifier object that should be used for supervised
         learning.
    seed: List of Tweet objects that are in the initial seed set.
    training: List of Tweet objects to be used for training the model.
              Represents the unlabeled pool.
    budget: Number of instances to be chosen from <training> for learning.
    strats: List of strategies to be used. Must have the same length as
            <names>. Values are "uc" for uncertainty sampling or "c" for
            certainty sampling.
    test: List of Tweet objects to be used as hold-out test set. If None,
          the results for CV are plotted.

    Returns
    -------
    double, numpy.array
    Weighted F1 score for the given budget and corresponding confusion matrix.

    """
    # f1 = []
    CLASSES = ["negative", "neutral", "positive"]
    # Choose training set arbitrarily
    clf = clone(clf_)
    training_tweets, training_idc = _choose_training_instances(training, budget)
    X_u = [t.bow for t in training_tweets]
    y_u = [t.label for t in training_tweets]
    # X_u = np.array(X_u)
    # y_u = np.array(y_u)
    # Incorporate seed set into training set
    X_u.extend([t.bow for t in seed])
    y_u.extend([t.label for t in seed])
    X_u = np.array(X_u)
    y_u = np.array(y_u)
    print "labels in training", len(y_u)
    clf.fit(X_u, y_u)
    X_t = []
    y_t = []
    # Use remaining instances as test set assuming no separate set is
    #  specified
    if test is None:
        for idx, t in enumerate(tweets):
            if idx not in training_idc:
                X_t.append(t.bow)
                y_t.append(t.label)
    else:
        X_t = np.array([t.bow for t in test])
        y_t = np.array([t.label for t in test])
    preds = clf.predict(X_t)
    cm = confusion_matrix(y_t, preds, labels=np.array(CLASSES))

    _, avg_f1 = _calculate_f1(cm, np.array(CLASSES))
    # f1.append(avg_f1)
    print "Overall Average F1", avg_f1
    # print "all average f1 scores for the different budgets", f1
    return avg_f1, cm


def test_different_seed_set_sizes(
        clfs, training, budgets, strats, al_types, seed_set_sizes, out_dir,
        test=None, seed=42, k=10, repetitions=3):
    """
    Creates a learning curve plot per seed set size.

    Parameters
    ----------
    clfs: List of SciKit classifier objects that should be used for AL.
    training: List of Tweet objects to be used for training the model.
              Represents the unlabeled pool.
    budgets: List of budgets to be used for building learners.
    strats: List of strategies to be used. Must have the same length as
            <names>. Values are "uc" for uncertainty sampling or "c" for
            certainty sampling.
    al_types: List of types to be used. It could either be "confidence" or
              "margin". Must have the same length as <strats>.
    seed_set_sizes: List of numbers of instances to be used in seed sets.
    out_dir: Directory in which the results should be stored.
    test: List of Tweet objects to be used as hold-out test set. If None,
          the results for CV are plotted.
    seed: Seed value for PRNG.
    k: Number of folds in CV.
    repetitions: Number of times a randomized set set is created for
                 averaging the results over multiple runs.
    """
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    for seed_set_size in seed_set_sizes:
        dst_dir = os.path.join(out_dir, str(seed_set_size))
        test_different_strategies_with_same_classifier_and_seed_set(
            clfs, training, budgets, strats, al_types, seed_set_size, dst_dir,
            test=test, seed=seed, k=k, repetitions=repetitions)


def test_different_strategies_with_same_classifier_and_seed_set(
        clfs, training, budgets, strats, al_types, seed_set_size, out_dir,
        test=None, seed=42, k=10, repetitions=3):
    """
    Plots the learning curves (w.r.t. weighted F1 score) to compare the
    performance of multiple AL strategies in one figure for different
    budgets. The seed set remains constant.

    Parameters
    ----------
    clfs: List of SciKit classifier objects that should be used for AL.
    training: List of Tweet objects to be used for training the model.
              Represents the unlabeled pool.
    budgets: List of budgets to be used for building learners.
    strats: List of strategies to be used. Must have the same length as
            <names>. Values are "uc" for uncertainty sampling or "c" for
            certainty sampling.
    al_types: List of types to be used. It could either be "confidence" or
              "margin". Must have the same length as <strats>.
    seed_set_size: Number of instances to be used in seed set.
    out_dir: Directory in which the results should be stored.
    test: List of Tweet objects to be used as hold-out test set. If None,
          the results for CV are plotted.
    seed: Seed value for PRNG.
    k: Number of folds in CV.
    repetitions: Number of times a randomized set set is created for
                 averaging the results over multiple runs.

    """
    random.seed(seed)
    # Each list represents the F1 score for all tested budgets per AL
    # strategy, e.g. [[budget1, budget2], [budget1, budget2]]
    strategy_scores = []
    # Name to be displayed in legend for AL strategy. It'll be UC or C plus
    # the classifier used
    names = []
    # A list representing F1 scores over all budgets
    baseline_scores = []
    # Create a directory under which all results will be stored
    if not os.path.exists(out_dir):
            os.makedirs(out_dir)
    # For each classifier
    for clf in clfs:
        clf_name = _get_classifier_name(clf)
        dst_dir = os.path.join(out_dir, clf_name)
        if not os.path.exists(dst_dir):
            os.makedirs(dst_dir)
        # For each strategy
        for idx, strat in enumerate(strats):
            print "\n---------------"
            print "AL strategy", strat
            print "---------------\n"
            al_scores = []
            base_scores = []
            cms = []
            base_cms = []
            # For each budget
            for budget in budgets:
                # Last confusion matrix -> it can't be averaged, so just return
                # the one from the last run to get an idea about the
                # classifier's weaknesses
                cm = None
                avg_f1_scores = []
                avg_base_f1_scores = []
                # Average F1 over multiple seed sets
                for rep in xrange(repetitions):
                    seed += 1
                    clf_cpy = clone(clf)
                    seed_set, unlabeled = create_seed_set_and_training_set(
                        training, seed_set_size)
                    print "seed set size", len(seed_set)
                    print "unlabeled pool", len(unlabeled)
                    # Baseline method
                    f1_base, cm_base = \
                        compute_baseline2(clf_cpy, seed_set, unlabeled,
                                          budget, test=test)
                    # repetitions, just compute the single f1 score
                    if strat == "uc":
                        uc = UncertaintySampling(
                            clf_cpy, seed_set, budget, seed=seed,
                            al_type=al_types[idx])
                        f1, cm = uc.cv_uncertainty(unlabeled, k=k, test=test)
                    if strat == "c":
                        uc = CertaintySampling(
                            clf_cpy, seed_set, budget, seed=seed,
                            al_type=al_types[idx])
                        f1, cm = uc.cv_certainty(unlabeled, k=k, test=test)
                    avg_f1_scores.append(f1)
                    avg_base_f1_scores.append(f1_base)
                avg_f1_score = sum(avg_f1_scores) / float(len(avg_f1_scores))
                print "base stuff", avg_base_f1_scores, type(avg_base_f1_scores)
                avg_base_f1_score = sum(avg_base_f1_scores) / float(len(
                    avg_base_f1_scores))
                print "average F1 score", avg_f1_scores, avg_f1_score
                al_scores.append(avg_f1_score)
                base_scores.append(avg_base_f1_score)
                cms.append(cm)
                base_cms.append(cm_base)

            # Now store the results obtained
            # 1. Find the best run in terms of F1 score and retain the
            # respective confusion matrix
            best_f1, best_cm, best_budget = find_best_budget(cms, al_scores,
                                                             budgets)
            best_f1_base, best_cm_base, best_budget_base = find_best_budget(
                base_cms, base_scores, budgets)
            # 2. Store name of strategy for plot
            legend_name = "{}-{}({})(F1:{:.2f})".format(
                strat.upper(), al_types[idx], clf_name, best_f1)
            names.append(legend_name)
            # 2.1. Store results to be reused for later purposes
            # F1-scores of AL method
            # Confusion matrices of AL method
            # Baseline F1-scores
            # Baseline confusion matrices
            # Seed set size
            # Investigated budgets
            base_name = "{}-{}({})(F1:{:.2f})".format(
                "Baseline Random", al_types[idx], clf_name, best_f1_base)
            results = [
                # Entry for AL score
                [al_scores, cms, seed_set_size, budgets, legend_name],
                # Entry for baseline
                [base_scores, base_cms, seed_set_size, budgets, base_name]
            ]
            dst = os.path.join(
                dst_dir, "{}_{}_reuse.txt".format(
                    strat, al_types[idx]))
            with open(dst, "wb") as f:
                cPickle.dump(results, f)
            # 3. Store averaged weighted F1 scores
            strategy_scores.append(al_scores)
            # 4. Store averaged weighted F1 scores of baseline. Storing it
            # once is sufficient!
            baseline_scores = base_scores
            # 5. Store confusion matrix at budget where best F1 score was
            # achieved
            dst = os.path.join(
                dst_dir, "{}_{}_different_budgets_best_results.txt".format(
                    strat, al_types[idx]))

            # 6. Store results in file
            with open(dst, "wb") as f:
                f.write("Best weighted F1 score with budget {}: {}\n".format(
                    best_budget, best_f1))
                _write_confusion_matrix(best_cm, ["negative", "neutral",
                                                  "positive"], f)
                # Store classifier parameters
                for classifier in clfs:
                    f.write("Classifier: " + clf_name + "\n")
                    f.write(str(classifier.get_params()))
                    f.write("\n")
    dst = os.path.join(out_dir, "different_budgets" + ".png")
    # Add baseline results at the end
    print "al scores", len(al_scores)
    print al_scores
    print "baseline scores", len(baseline_scores)
    print baseline_scores
    strategy_scores.append(baseline_scores)
    # Add name
    names.append("Baseline Random")

    plot_f1(strategy_scores, budgets, names,
            title="on Test Set (Seed set size: {})".format(seed_set_size),
            dst=dst)


def find_best_budget(cms, f1_scores, budgets):
    """
    Finds the budget that optimizes F1 score.

    Parameters
    ----------
    cms: List of confusion matrices for the respective budgets.
    f1_scores: List of F1 scores for the respective budgets.
    budgets: Budgets for which the performance of the AL strategy was analyzed.

    Returns
    -------
    double, np_array, int.
    Best F1 score, respective confusion matrix, best budget where F1 score
    was achieved.

    """
    best_f1 = 0.0
    best_cm = []
    best_budget = 0
    for idx in xrange(len(budgets)):
        f1 = f1_scores[idx]
        if f1 > best_f1:
            best_f1 = f1
            best_cm = cms[idx]
            best_budget = budgets[idx]
    return best_f1, best_cm, best_budget


if __name__ == "__main__":
    data_set = "../preprocessed_datasets/alt_test_cpd_0_4_3_train_full.txt"
    test_set = "../preprocessed_datasets/alt_test_cpd_0_4_3_test_full.txt"
    with open(data_set, "rb") as f:
        tweets = cPickle.load(f)
    with open(test_set, "rb") as f:
        test_tweets = cPickle.load(f)
    seed_instances = 6
    # print choose_seed_set(tweets, seed_instances)
    # print choose_seed_set(tweets, seed_instances)
    # print choose_seed_set(tweets, seed_instances)
    # for t in seed_set:
    #     print t.text
    # budget = 5
    mnb = MultinomialNB()
    mnb = OneVsRestClassifier(mnb)
    # Use full budget
    # budgets = [i + 1 for i in (xrange(len(tweets) - seed_instances))]
    # All of these following lists must have the same number of entries to work!
    # strats = ["uc", "uc", "c", "c"]
    # al_types = ["margin", "confidence", "margin", "confidence"]
    # strats = ["uc", "uc", "c", "c"]
    # al_types = ["margin", "confidence", "margin", "confidence"]
    # lr = LogisticRegression(multi_class="ovr", penalty="l1", dual=False,
    #                         C=0.1, class_weight="auto", max_iter=1000,
    #                         solver="liblinear", tol=0.001)
    # clfs = [mnb, lr]

    # Test stuff
    lr = LogisticRegression(multi_class="ovr", penalty="l2", dual=False,
                            C=1.0, class_weight="auto", max_iter=1000,
                            solver="lbfgs", tol=0.001)
    clfs = [mnb, lr]
    strats = ["uc", "uc", "c", "c"]
    al_types = ["margin", "confidence", "margin", "confidence"]
    strats = ["uc", "uc"]
    al_types = ["margin", "confidence"]

    out_dir = "test"
    out_dir = os.path.join(out_dir, str(datetime.now().time()))
    # test_different_strategies_with_same_classifier_and_seed_set(
    #     clfs, tweets, budgets, strats, al_types, seed_instances, out_dir,
    #     test=test_tweets, seed=42, k=3, repetitions=10)

    # seed_instances = [3, 6, 12]
    # test_different_seed_set_sizes(
    #     clfs, tweets, budgets, strats, al_types, seed_instances, out_dir,
    #     test=test_tweets, seed=42, k=3, repetitions=10)
    #
    # # Test for alt_full_with_3_feature_spaces_43_features_cpd_0_4_fixed_
    # lr = LogisticRegression(multi_class="ovr", penalty="l2", dual=False,
    #                         C=0.1, class_weight="auto", max_iter=1000,
    #                         solver="lbfgs", tol=0.001)

    mnb = MultinomialNB()
    mnb = OneVsRestClassifier(mnb)
    clfs = [mnb]
    # strats = ["uc", "uc", "c", "c"]
    # al_types = ["margin", "confidence", "margin", "confidence"]
    strats = ["uc", "uc", "c", "c"]
    al_types = ["margin", "confidence", "margin", "confidence"]
    out_dir = "test_reuseability"
    out_dir = os.path.join(out_dir, str(datetime.now().time()))
    # Use full feature set
    # data_set = "../preprocessed_datasets" \
    #            "/alt_submission_cpd_0_6_10_train_full.txt"
    # test_set = "../preprocessed_datasets" \
    #            "/alt_submission_cpd_0_6_10_test_full.txt"
    # Use reduced feature set

    data_set = "../preprocessed_datasets" \
               "/alt_submission_cpd_0_6_10_train_reduced.txt"
    test_set = "../preprocessed_datasets" \
               "/alt_submission_cpd_0_6_10_test_reduced.txt"

    with open(data_set, "rb") as f:
        tweets = cPickle.load(f)
    with open(test_set, "rb") as f:
        test_tweets = cPickle.load(f)
    seed_instances = [400, 500, 600]
    budgets = [100, 200, 300, 400, 500]
    test_different_seed_set_sizes(
        clfs, tweets, budgets, strats, al_types, seed_instances, out_dir,
        test=test_tweets, seed=42, k=10, repetitions=3)
