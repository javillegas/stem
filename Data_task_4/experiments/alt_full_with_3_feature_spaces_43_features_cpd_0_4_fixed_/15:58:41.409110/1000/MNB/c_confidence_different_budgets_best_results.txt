Best weighted F1 score with budget 1000: 0.466973800976

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 161      | 113      | 85       | 
| neutral  | 171      | 244      | 270      | 
| positive | 100      | 204      | 451      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
