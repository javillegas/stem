Best weighted F1 score with budget 1000: 0.475424803905

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 102      | 187      | 70       | 
| neutral  | 99       | 346      | 240      | 
| positive | 57       | 272      | 426      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
