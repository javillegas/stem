Best weighted F1 score with budget 800: 0.449171350932

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 41       | 227      | 91       | 
| neutral  | 50       | 356      | 279      | 
| positive | 25       | 268      | 462      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
