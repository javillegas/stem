Best weighted F1 score with budget 900: 0.470573061316

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 74       | 199      | 86       | 
| neutral  | 93       | 346      | 246      | 
| positive | 55       | 261      | 439      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
