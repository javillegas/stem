"""
Try AdaBoost.
"""
from sklearn.ensemble import AdaBoostClassifier

from Data_task_4.experiments.run_experiments_own_bow import *
from Data_task_4.io.input_output import open_dataset


def run_experiments(base_config_dir):
    """
    Executes all specified experiments for a task and stores the outcomes in
    the respective output directories.

    Parameters
    ----------
    base_config_dir: Base directory in which all configuration files for the
    experiments are stored.

    """
    # This is a list of entries each config file MUST include, otherwise it
    # can't be read in successfully and the experiment is bound to fail.
    MANDATORY = ["input", "preprocess", "classifier", "output"]
    # For each config file
    for conf in os.listdir(base_config_dir):
        config_path = os.path.join(base_config_dir, conf)
        # Omit directories
        if os.path.isfile(config_path):
            # Read in the parameters
            in_params, pre_params, train_params, eval_params, out_params = \
                read_config(config_path)
            print "\n------------Reading in parameters completed-------------\n"
            output_dir = out_params[0]["output"]["base"]
            algo = train_params[0]["classifier"]["name"]
            now = datetime.datetime.now()
            now = str(now).split(".")[0]
            name = str(now) + "_" + algo
            output_dir = os.path.join(output_dir, name)
            # Store the config file in the experiment subfolder for
            # reproducibility
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)
            shutil.copy(config_path, os.path.join(output_dir, "config.txt"))
            tweet_path = in_params[0]["input"].get("tweets", None)
            clean_path = in_params[0]["input"]["raw"]
            print "clean path", clean_path
            print "tweet path", tweet_path
            tweets = open_dataset(tweet_path)
            print "Read in: {} tweets for subtask a".format(len(tweets))
            print "Entire corpus to be used for training/testing comprises {" \
                  "} tweets.".format(len(tweets))
            print "\n----------------Pre-processing completed----------------\n"
            # classifier = get_classifier(train_params)
            nb = MultinomialNB()
            classifier = AdaBoostClassifier(n_estimators=600,
                                            learning_rate=1.5,
                                            algorithm="SAMME")
            # classifier = RandomForestClassifier(n_estimators=500, n_jobs=10)
            print "\n----------------Classifier instantiated-----------------\n"
            print "classifier to be used is", type(classifier)
            evaluate(classifier, tweets, eval_params,
                     pre_params, output_dir)
            print "\n-----------------Evaluation completed-------------------\n"


if __name__ == "__main__":
    run_experiments("configs")