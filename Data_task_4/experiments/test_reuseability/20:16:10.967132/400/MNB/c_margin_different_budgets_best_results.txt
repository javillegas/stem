Best weighted F1 score with budget 500: 0.454338899287

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 165      | 102      | 92       | 
| neutral  | 177      | 220      | 288      | 
| positive | 90       | 207      | 458      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
