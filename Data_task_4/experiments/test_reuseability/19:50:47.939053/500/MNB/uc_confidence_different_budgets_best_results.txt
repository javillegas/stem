Best weighted F1 score with budget 400: 0.468822139947

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 84       | 158      | 117      | 
| neutral  | 76       | 255      | 354      | 
| positive | 38       | 164      | 553      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
