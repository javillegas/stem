Best weighted F1 score with budget 100: 0.446891321948

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 124      | 98       | 137      | 
| neutral  | 123      | 139      | 423      | 
| positive | 69       | 106      | 580      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
