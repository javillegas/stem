Best weighted F1 score with budget 400: 0.440085628273

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 158      | 105      | 96       | 
| neutral  | 172      | 228      | 285      | 
| positive | 110      | 177      | 468      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
