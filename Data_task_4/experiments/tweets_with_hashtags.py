"""
Counts how many tweets actually use hashtags.
"""
from collections import Counter

from Data_task_4.io.input_output import open_dataset

if __name__ == "__main__":
    dataset_path = "../semeval2016_task4/dev/tweets.txt"
    tweets = open_dataset(dataset_path)
    hashtags = 0

    for tweet in tweets:
        if "#" in tweet.text:
            hashtags += 1
    print "{:.2f}% of the tweets contain hashtags ({} out of {})".format(
        100.0*hashtags/len(tweets), hashtags, len(tweets))

    counts = Counter()
    for tweet in tweets:
        if "#" in tweet.text:
            counts[tweet.label] += 1
    print "{:.2f}% of the hashtags belong to 'positive' ({} out of {})".format(
        100.0*counts["positive"]/len(tweets), counts["positive"], len(tweets))
    print "{:.2f}% of the hashtags belong to 'neutral' ({} out of {})".format(
        100.0*counts["neutral"]/len(tweets), counts["neutral"], len(tweets))
    print "{:.2f}% of the hashtags belong to 'negative' ({} out of {})".format(
        100.0*counts["negative"]/len(tweets), counts["negative"], len(tweets))
