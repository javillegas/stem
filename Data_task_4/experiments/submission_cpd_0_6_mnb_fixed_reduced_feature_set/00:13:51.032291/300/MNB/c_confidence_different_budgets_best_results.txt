Best weighted F1 score with budget 400: 0.453035771871

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 219      | 65       | 75       | 
| neutral  | 262      | 164      | 259      | 
| positive | 165      | 146      | 444      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
