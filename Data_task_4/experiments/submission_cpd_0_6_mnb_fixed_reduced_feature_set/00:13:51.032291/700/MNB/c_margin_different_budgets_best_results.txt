Best weighted F1 score with budget 500: 0.473179561138

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 116      | 204      | 39       | 
| neutral  | 110      | 398      | 177      | 
| positive | 50       | 385      | 320      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
