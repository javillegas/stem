Best weighted F1 score with budget 500: 0.461216946873

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 99       | 218      | 42       | 
| neutral  | 84       | 395      | 206      | 
| positive | 36       | 354      | 365      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
