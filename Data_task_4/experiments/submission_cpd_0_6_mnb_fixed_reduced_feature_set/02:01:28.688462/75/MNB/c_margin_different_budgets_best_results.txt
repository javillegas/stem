Best weighted F1 score with budget 200: 0.427113240433

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 22       | 209      | 128      | 
| neutral  | 62       | 268      | 355      | 
| positive | 62       | 151      | 542      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
