import csv
import sys

from Data_task_4.preprocess.preprocessing_for_kousar import preprocess


def read_seed_list(p):
    dic = {}
    with open(p, "rb") as f:
        for w in f:
            w = w.strip()
            dic[w.lower()] = None
    return dic


def read_dataset(p, two=True):
    """
    Reads in the dataset.

    Parameters
    ----------
    p: Path to the input file.
    two: If true, the binary label will be chosen. Otherwise the one for 3
    classes is selected.

    Returns
    -------
    List of (int, string, string) triples.
    The first entry represents a Tweet ID, the 2nd the actual text and the
    3rd its label.

    """
    tweets = []
    with open(p, "rb") as f:
        data = csv.reader(f, delimiter='\t', quoting=csv.QUOTE_NONE)
        for t in data:
            # Labels: first label for 3-class problem
            if two:
                t_id, text, label = t[0], t[1], t[2]
            else:
                t_id, text, label = t[0], t[1], t[3]
            text = text.strip()
            # Strip off "" from text and label
            text = text[1:-1].strip()
            label = label[1:-1].strip()
            tweets.append((t_id, text, label))
    return tweets


def to_arff(tweets, dst, only_two_classes=True):
    """

    Converts file under <p> into .arff format and saves it under <dst>.

    Parameters:
    -----------
    tweets: List of data to be stored. Each element represents a tweets.
    Order of inputs is preserved, i.e. the order of the attributes MUST be
    according to the ARFF header!
    dst: Path under which arff file will be stored.
    only_two_classes: True, if only "influential" and "noninfluential" should be used as labels for a user instead of all 4 values.
    for_libsvm: True, if dataset needs to be created for LibSVM. Since parameters of this algorithm need to be optimized, the class label must be represented as (1,-1).

    """
    with open(dst, "w") as f:
        if only_two_classes:
            f.write("""
            @RELATION features\n
            @ATTRIBUTE id integer
            @ATTRIBUTE tweet_length integer
            @ATTRIBUTE positive_seed_words integer
            @ATTRIBUTE negative_seed_words integer
            @ATTRIBUTE positive_emoticons integer
            @ATTRIBUTE negative_emoticons integer
            @ATTRIBUTE exclamation_marks integer
            @ATTRIBUTE question_marks integer
            @ATTRIBUTE negative_dictionary_words integer
            @ATTRIBUTE positive_dictionary_words integer
            @ATTRIBUTE elongated_words integer
            @ATTRIBUTE caps_words integer
            @ATTRIBUTE label {A, D}

            @DATA
            """)
        else:
            f.write("""
            @RELATION features\n
            @ATTRIBUTE id integer
            @ATTRIBUTE tweet_length integer
            @ATTRIBUTE positive_seed_words integer
            @ATTRIBUTE negative_seed_words integer
            @ATTRIBUTE positive_emoticons integer
            @ATTRIBUTE negative_emoticons integer
            @ATTRIBUTE exclamation marks integer
            @ATTRIBUTE question marks integer
            @ATTRIBUTE negative_dictionary_words integer
            @ATTRIBUTE positive_dictionary_words integer
            @ATTRIBUTE elongated_words integer
            @ATTRIBUTE caps_words integer
            @ATTRIBUTE label {A, D, SD}

            @DATA
            """)
        # Number of features per tweet
        feature_n = len(tweets[0]) - 1
        for idx, tweet in enumerate(tweets):
            # No new line for first line
            if idx != 0:
                f.write("\n")
            for no, feature in enumerate(tweet):
                # Last item doesn't need a trailing comma
                if no == feature_n:
                    f.write(str(feature))
                else:
                    f.write(str(feature) + ",")


def create_dataset(data, dst, pos_seeds, neg_seeds, data_three_class=None,
                   three_class_dst=None):
    """
    Creates the dataset for Kousar's experiment.

    Parameters
    ----------
    data: List of triples representing tweets: (id, text, label).
    dst: Output path for created 2-class problem ARFF file.
    pos_seeds: Dictionary of positive seed words
    neg_seeds: Dictionary of negative seed words
    data_three_class: If specified, also creates an ARFF file for the
    respective 3-class problem. It takes as input the same list as <data>,
    but the labels differ.
    three_class_dst: Output path for created ARFF file for 3-class problem.

    """
    cnt = 0
    # Each tweet is represented as a list of features
    # [[feature1, feature2,...], [feature1, feature2...]]
    features_two = []
    features_three = []
    create_three_class = False
    # Also create an ARFF for the 3-class problem
    if data_three_class is not None and three_class_dst is not None:
        create_three_class = True
    for d in data:
        t_id = d[0]
        text = d[1]
        label = d[2]
        tweet_len, pos_seed_words, neg_seed_words, pos_emos, neg_emos, \
        exclamation_marks, question_marks, tmp = preprocess(text, pos_seeds,
                                                            neg_seeds)
        neg_words, pos_words, elongated_counter, uppercase_counter = \
            tmp[0], tmp[1], tmp[2], tmp[3],
        # print "len: {}\npos_seed:{}\nneg_seed:{}\npos_emos:{}\nneg_emos:{" \
        #       "}\n!:{}\n?:{}\nneg_words:{}\npos_words:{}\nelo:{}\nupper:{" \
        #       "}\nid:{}\nlabel:{}" \
        #       "".format(
        #     tweet_len, pos_seed_words, neg_seed_words, \
        #     pos_emos, \
        #          neg_emos, \
        #     exclamation_marks, question_marks, \
        # neg_words, pos_words, elongated_counter, uppercase_counter, t_id, label)
        # Careful! Order is the same as in ARFF
        tmp = [t_id, tweet_len, pos_seed_words, neg_seed_words,
                         pos_emos, neg_emos, exclamation_marks,
                         question_marks, neg_words, pos_words,
                         elongated_counter, uppercase_counter, label]
        features_two.append(tmp)
        if create_three_class:
            # Replace the binary label with the label of the 3-class problem
            tmp = tmp[:]
            tmp[-1] = data_three_class[cnt][-1]
            features_three.append(tmp)
        cnt += 1
        prog = str(1.0*cnt/4000*100)
        sys.stdout.write('{}% ({}/4000)\r'.format(prog, cnt))
        sys.stdout.flush()
    to_arff(features_two, dst)
    if create_three_class:
        to_arff(features_three, three_class_dst, only_two_classes=False)


if __name__ == "__main__":
    neg = "../NegativeSeedWords.txt"
    pos = "../PositiveSeedWords.txt"
    tweet_p = "../FinalDatasetStefan.csv"
    dst_two = "../kousar_dataset_two_classes.arff"
    dst_three = "../kousar_dataset_three_classes.arff"

    # List of seed words
    neg_seeds = read_seed_list(neg)
    pos_seeds = read_seed_list(pos)

    data_two = read_dataset(tweet_p)
    data_three = read_dataset(tweet_p, two=False)
    create_dataset(data_two, dst_two, pos_seeds, neg_seeds,
                   data_three_class=data_three, three_class_dst=dst_three)
