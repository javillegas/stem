Best weighted F1 score with budget 14: 0.348484848485

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 2        | 0        | 0        | 
| neutral  | 0        | 1        | 3        | 
| positive | 2        | 1        | 2        | 
