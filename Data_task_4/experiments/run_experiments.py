import ast
import os
import cPickle
from sklearn.cross_validation import KFold, ShuffleSplit, \
    StratifiedKFold, Bootstrap, train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import roc_curve, auc, f1_score, confusion_matrix
from sklearn.multiclass import OneVsOneClassifier, OneVsRestClassifier
from sklearn.preprocessing import label_binarize, StandardScaler, MinMaxScaler
from sklearn.linear_model import LogisticRegression
from Data_task_4.io.input_output import open_cleansed_tsv, open_tweets_tsv, \
    open_dataset
from Data_task_4.preprocess.filters import to_bag_of_words, \
    _determine_preprocessor
from Data_task_4.settings import INVALID_TWEETS
from parser.config_parse import parse_config_file
from ast import literal_eval
from Data_task_4 import settings
from nltk.corpus import stopwords
from sklearn.naive_bayes import MultinomialNB
import numpy as np
import matplotlib.pyplot as plt
import shutil
import datetime
from sklearn.externals import joblib
from sklearn.svm import SVC


def read_config(config_path):
    """
    Read in a configuration file and return the parameters.

    Parameters
    ----------
    config_path: Path to a configuration file.

    Returns
    -------
    List, list, list, list, list
    Returns a quintuple of lists where each one corresponds to a number of
    parameters for input, preprocessing, training, evaluation and output (in
    that order). Most of the lists contain dictionaries to access the
    parameters, but for preprocessing lists of tuples are used to follow the
    order specified in the config file.

    """
    out = []
    inp = []
    train = []
    evalu = []
    pre = []
    params = parse_config_file(config_path)
    print params
    for param_set in params:
        if "input" in param_set:
            inp.append(param_set)
        if "preprocess" in param_set:
            pre.append(param_set)
        if "classifier" in param_set:
            train.append(param_set)
        if "evaluation" in param_set:
            evalu.append(param_set)
        if "output" in param_set:
            out.append(param_set)
    print "in", inp
    print "pre", pre
    print "train", train
    print "eval", evalu
    print "out", out
    return inp, pre, train, evalu, out


def _filter_tweets(tweets, remove_invalid_tweets=False,
                   ignore_error_messages=False):
    """
    Removes some undesired tweets according to one's needs.

    Parameters
    ----------
    tweets: List of tweet objects.
    remove_invalid_tweets: If True removes invalid tweets from the dataset such
                           that they won't be used for training or anything at
                           all. If False those tweets won't be treated
                           differently. Default: False
    ignore_error_messages: If True, tweets for which the texts couldn't be
                           retrieved will be considered, but the error
                           messages won't count toward the BoW frequencies.
                           If False nothing happens. Default: False

    Returns
    -------
    List of tweet objects that contains only valid tweets.

    """
    # Nothing to do
    if not remove_invalid_tweets and not ignore_error_messages:
        return tweets
    filtered_tweets = []
    dummy = ""
    # Dictionary containing all messages that are deemed invalid
    INVALID_TWEETS = {}
    # Convert keys to lowercase for this function
    for txt in settings.INVALID_TWEETS:
        INVALID_TWEETS[txt.lower()] = None

    for tweet in tweets:
        is_valid = True
        # Ignore all tweets for which no texts could be retrieved
        # Test this at first because after potentially replacing the text,
        # it's impossible to find out
        if remove_invalid_tweets:
            if tweet.text in INVALID_TWEETS:
                is_valid = False
        # Use placeholder message
        if ignore_error_messages:
            if tweet.text in INVALID_TWEETS:
                tweet.text = dummy
        if is_valid:
            filtered_tweets.append(tweet)
    return filtered_tweets


def _get_vectorizer(binary=False, min_df=0.0, max_df=1.0,
                    max_features=None, lowercase=True,
                    stop_words=None, n_gram_range=None, strip_accents=None,
                    stemmer=None, stemmer_lang=None):
    """
    Creates a CountVectorizer object according to the parameters.

    Parameters
    ----------
    binary: If True use presence of words instead of word frequency when
            computing BoW. If False, use word frequency.
    min_df: Cut-off words occurring less than min_df times in the corpus.
            Range is [0-1].
    max_df: Cut-off words occurring more often than max times in the corpus.
            Range is [0.7-1].
    max_features: Number of columns/words to be used in BoW representation.
                  None implies unlimited number.
    lowercase: Convert texts into lowercase before transforming corpus into
               BoW if True. If False texts will be used as is.
    stop_words: Words to be ignored when creating BoW representation. Could
                be either a list, a string or None to exclude no words.
    n_gram_range: A tuple specifying which n-grams should be included in the
                  BoW representation. Borders of the tuple are included.
    strip_accents: Remove accents during the preprocessing step.Allowed
                   values are {ascii', 'unicode', None}.
    stemmer: For stemming words. Valid values are "porter", "snowball" and None.
    stemmer_lang: lang: Language to be used for stemming, e.g. "english".

    Returns
    -------
    Initialised CountVectorizer object.

    """
    # Convert "None" to None
    if max_features == "None":
        max_features = None
    if stop_words == "None":
        stop_words = None
    if n_gram_range == "None":
        n_gram_range = None
    if strip_accents == "None":
        strip_accents = None
    if stemmer == "None":
        stemmer = None
    if stemmer_lang == "None":
        stemmer_lang = None
    args = {"stemmer": stemmer, "lang": stemmer_lang}
    # see https://www.kaggle.com/c/word2vec-nlp-tutorial/details/part-1-for
    # -beginners-bag-of-words
    if n_gram_range is None:
        # Instantiate a new vectorizer because none exists so far
        vectorizer = CountVectorizer(
            analyzer="word",
            tokenizer=None,
            # Apply this preprocessor to every tweet text separately
            preprocessor=lambda text: _determine_preprocessor(text, **args),
            stop_words=stop_words,
            max_features=max_features,
            lowercase=lowercase,
            min_df=min_df,
            max_df=max_df,
            binary=binary,
            strip_accents=strip_accents,
            # http://stackoverflow.com/questions/20717641/countvectorizer-i
            # -not-showing-up-in-vectorized-text -> without changing the
            # token pattern, any word consisting of 1 letter wouldn't be
            # vectorized
            token_pattern="(?u)\\b\\w+\\b",
        )
    else:
        # Instantiate a new vectorizer because none exists so far
        vectorizer = CountVectorizer(
            analyzer="word",
            tokenizer=None,
            preprocessor=lambda text: _determine_preprocessor(text, **args),
            stop_words=stop_words,
            max_features=max_features,
            lowercase=lowercase,
            min_df=min_df,
            max_df=max_df,
            binary=binary,
            strip_accents=strip_accents,
            ngram_range=n_gram_range,
            token_pattern="(?u)\\b\\w+\\b",
        )
    return vectorizer


def preprocess_tweets(tweets, params, selected_tweets=None, vectorizer=None):
    """
    Preprocess the tweets according to the configuration parameters.

    Parameters
    ----------
    tweets: List of tweet objects.
    params: Parameters for preprocessing.
    selected_tweets: List with tweets indices to be considered. If None,
                     then all tweets are used, otherwise only the indexed
                     tweets will be used from tweets.
    vectorizer: If None, it means a new one should be trained on the
                vocabulary of tweets. Otherwise an instance of a vectorizer (
                with a vocabulary) must be passed in. This is useful for
                creating the exact same BoW representation of the training
                set for the test set as well.

    Returns
    -------
    List of tweet objects that were preprocessed accordingly.

    """
    # Use only the tweets indexed in selected_tweets as corpus
    if selected_tweets is not None:
        tweets = [tweets[i] for i in selected_tweets]
    # Variables are all initialized with default values that are used anyway
    # Bag-of-word transformation
    use_bow = False
    lowercase = True
    stop_words_sci = "None"
    stop_words_nltk = "None"
    stemmer = "None"
    stemmer_lang = "None"
    max_df = 1.0
    min_df = 1.0
    max_features = "None"
    binary = False
    remove_punctuation = False
    strip_accents = "None"
    n_gram_range = "None"
    pos_tag = False
    replace_urls = False
    replace_handles = False
    replace_hashtags = False
    treat_abbreviations = False
    remove_weird_chars = False
    handle_slang = False
    # ignore_invalid_tweets = False
    # ignore_error_messages = False
    stop_words = "None"
    # defined in the config and extract them first
    for param in params[0]["preprocess"]:   # Param is a tuple: (key, value)
        # Read in all parameters for BoW transformation and applied it
        if param[0] == "bow":
            use_bow = True
            for p in param[1]:
                # Get the key and value from the string
                k, v = p.split(":")
                k = k.split()[0]
                v = v.split()[0]
                if k == "lowercase":
                    if v == "False":
                        lowercase = False
                if k == "stop_words_sci":
                    if v != "None":
                        stop_words_sci = v
                if k == "stop_words_nltk":
                    if v != "None":
                        stop_words_nltk = stopwords.words(v)
                if k == "max_df":
                    # Float and ints have different meaning here -> careful!
                    if v != "1.0":
                        # float number
                        try:
                            max_df = int(v)
                        except ValueError:
                            max_df = float(v)
                if k == "min_df":
                    # Float and ints have different meaning here -> careful!
                    if v != "1.0":
                        # float number
                        try:
                            min_df = int(v)
                        except ValueError:
                            min_df = float(v)
                if k == "binary":
                    if v == "True":
                        binary = True
                if k == "max_features":
                    if v != "None":
                        max_features = int(v)
                # if k == "ignore_invalid_tweets":
                #     if v == "True":
                #         ignore_invalid_tweets = True
                # if k == "ignore_error_messages":
                #     if v == "True":
                #         ignore_error_messages = True
                if k == "n_gram_range":
                    if v != "None":
                        n_gram_range = literal_eval(v)
                if k == "strip_accents":
                    if v != "None":
                        strip_accents = v
                if k == "stemmer":
                    if v != "None":
                        stemmer = v
                if k == "stemmer_lang":
                    if v != "None":
                        stemmer_lang = v
            # Give higher precedence to NLTK stop word list if both lists are
            #  set
            if stop_words_nltk != "None":
                stop_words = stop_words_nltk
            elif stop_words_sci != "None":
                stop_words = stop_words_sci
            print "BoW", use_bow
            print "lowercase", lowercase
            print "stop_words sci", stop_words_sci
            print "stop_words nltk", stop_words_nltk
            print "stop_words", stop_words
            print "max_df", max_df
            print "min_df", min_df
            print "binary", binary
            print "max_features", max_features
            # print "discard invalid tweets?", ignore_invalid_tweets
            # print "ignore error messages in BoW?", ignore_error_messages
            print "n-gram range", n_gram_range
            print "strip_accents", strip_accents
            print "stemmer", stemmer
            print "stemmer language", stemmer_lang
            # Before creating BoW, make sure only desired tweets are passed
            # to that method
            # filtered_tweets = _filter_tweets(
            #     tweets,
            #     remove_invalid_tweets=ignore_invalid_tweets,
            #     ignore_error_messages=ignore_error_messages,
            # )
            if use_bow:
                # If a no vectorizer exists so far
                do_train = True if vectorizer is None else False
                if vectorizer is None:
                    vectorizer = _get_vectorizer(
                        binary=binary,
                        min_df=min_df,
                        max_df=max_df,
                        max_features=max_features,
                        lowercase=lowercase,
                        stop_words=stop_words,
                        n_gram_range=n_gram_range,
                        strip_accents=strip_accents,
                        stemmer=stemmer,
                        stemmer_lang=stemmer_lang,
                    )
                tweets, vectorizer = to_bag_of_words(
                    tweets,
                    vectorizer=vectorizer,
                    do_train=do_train
                )

        if param[0] == "remove_punctuation":
            if param[1][0] == "True":
                remove_punctuation = True
        if param[0] == "pos_tag":
            if param[1][0] == "True":
                pos_tag = True
        if param[0] == "replace_urls":
            if param[1][0] == "True":
                replace_urls = True
        if param[0] == "replace_handles":
            if param[1][0] == "True":
                replace_handles = True
        if param[0] == "replace_hashtags":
            if param[1][0] == "True":
                replace_hashtags = True
        if param[0] == "treat_abbreviations":
            if param[1][0] == "True":
                treat_abbreviations = True
        if param[0] == "remove_weird_chars":
            if param[1][0] == "True":
                remove_weird_chars = True
        if param[0] == "handle_slang":
            if param[1][0] == "True":
                handle_slang = True

    print "The following configuration was used for preprocessing:"
    print "BoW", use_bow
    print "lowercase", lowercase
    print "stop_words", stop_words
    print "max_df", max_df
    print "min_df", min_df
    print "binary", binary
    print "remove_punctuation", remove_punctuation
    print "strip_accents", strip_accents
    print "n_gram_range", n_gram_range
    print "max_features", max_features
    print "pos_tag", pos_tag
    print "replace urls", replace_urls
    print "replace handles", replace_handles
    print "replace hashtags", replace_hashtags
    print "treat abbreviations", treat_abbreviations
    print "remove weird chars", remove_weird_chars
    print "slang", handle_slang
    print "stemmer", stemmer
    print "stemmer language", stemmer_lang

    return tweets, vectorizer


def _remove_invalid_tweets(tweets, params):
    """
    Removes some of the tweets depending on the user's preferences. Tweets
    are removed if they contain invalid messages, i.e. their texts couldn't
    be retrieved.

    Parameters
    ----------
    tweets: List of tweet objects.
    params: List of pre-processing parameters to be evaluated.

    Returns
    -------
    List of valid tweet objects.

    """
    ignore_invalid_tweets = False
    ignore_error_messages = False
    for param in params[0]["preprocess"]:
        print param
        print param[0]
        if param[0] == "ignore_invalid_tweets":
            if param[1][0] == "True":
                    ignore_invalid_tweets = True
            if param[0] == "ignore_error_messages":
                if param[1][0] == "True":
                    ignore_error_messages = True
    print "discard invalid tweets?", ignore_invalid_tweets
    print "ignore error messages in BoW?", ignore_error_messages
    return _filter_tweets(
                tweets,
                remove_invalid_tweets=ignore_invalid_tweets,
                ignore_error_messages=ignore_error_messages,
            )


def get_random_forest(params):
    """
    Instantiates a Random Forest classifier in scikit.

    Parameters
    ----------
    params: List of parameters to be used for instantiating the classifier.

    Returns
    -------
    RandomForest classifier from scikit.

    """
    n_estimators = 10
    criterion = "gini"
    max_features = "auto"
    max_depth = None
    min_samples_split = 2
    min_samples_leaf = 1
    min_weight_fraction_leaf = 0.0
    max_leaf_nodes = None
    bootstrap = True
    oob_score = True
    n_jobs = -1
    random_state = None
    verbose = 0
    warm_start = False
    class_weight = None

    for param, val in params[0]["classifier"].iteritems():
        if param == "n_estimators":
            n_estimators = int(val)
        if param == "criterion":
            criterion = val
        if param == "max_features":
            try:
                max_features = int(val)
            except ValueError:
                try:
                    max_features = float(val)
                except ValueError:
                    max_features = val
        if param == "max_depth":
            try:
                max_depth = int(val)
            except ValueError:
                max_depth = None
        if param == "min_samples_split":
            min_samples_split = int(val)
        if param == "min_samples_leaf":
            min_samples_leaf = int(val)
        if param == "min_weight_fraction_leaf":
            min_weight_fraction_leaf = float(val)
        if param == "max_leaf_nodes":
            try:
                max_leaf_nodes = int(val)
            except ValueError:
                max_leaf_nodes = None
        if param == "bootstrap":
            if val == "False":
                bootstrap = False
        if param == "oob_score":
            if val == "False":
                oob_score = False
        if param == "n_jobs":
            n_jobs = int(val)
        if param == "random_state":
            try:
                random_state = int(val)
            except ValueError:
                random_state = 42
        if param == "verbose":
            verbose = int(val)
        if param == "warm_start":
            if val == "True":
                warm_start = True
        if param == "class_weight":
            try:
                class_weight = ast.literal_eval(val)
            except ValueError:
                class_weight = None
    print "Random Forest parameters"
    print "n_estimators", n_estimators
    print "criterion", criterion
    print "max_features", max_features, type(max_features)
    print "max_depth", max_depth, type(max_depth)
    print "min_samples_split", min_samples_split
    print "min_samples_leaf", min_samples_leaf
    print "min_weight_fraction_leaf", min_weight_fraction_leaf
    print "max_leaf_nodes", max_leaf_nodes
    print "bootstrap", bootstrap
    print "oob_score", oob_score
    print "n_jobs", n_jobs, type(n_jobs)
    print "random_state", random_state
    print "verbose", verbose
    print "warm_start", warm_start
    print "class_weight (left out if equal weights)", class_weight, \
        type(class_weight)
    # Equal class weights
    if class_weight is None:
        print "equal class weight"
        return RandomForestClassifier(
            n_estimators=n_estimators,
            criterion=criterion,
            max_features=max_features,
            max_depth=max_depth,
            min_samples_split=min_samples_split,
            min_samples_leaf=min_samples_leaf,
            min_weight_fraction_leaf=min_weight_fraction_leaf,
            max_leaf_nodes=max_leaf_nodes,
            bootstrap=bootstrap,
            oob_score=oob_score,
            n_jobs=n_jobs,
            random_state=random_state,
            verbose=verbose,
            warm_start=warm_start,
        )
    else:
        # Weighted classes
        print "weighted classes"
        return RandomForestClassifier(
            n_estimators=n_estimators,
            criterion=criterion,
            max_features=max_features,
            max_depth=max_depth,
            min_samples_split=min_samples_split,
            min_samples_leaf=min_samples_leaf,
            min_weight_fraction_leaf=min_weight_fraction_leaf,
            max_leaf_nodes=max_leaf_nodes,
            bootstrap=bootstrap,
            oob_score=oob_score,
            n_jobs=n_jobs,
            random_state=random_state,
            verbose=verbose,
            warm_start=warm_start,
            class_weight=class_weight,
        )


def get_mnb(params):
    """
    Instantiates a Multinomial Naive Bayes classifier in scikit.

    Parameters
    ----------
    params: List of parameters to be used for instantiating the classifier.

    Returns
    -------
    MNB classifier from scikit.

    """
    learn_prior = True
    alpha = 1.0
    for param, val in params[0]["classifier"].iteritems():
        if param == "learn_prior":
            learn_prior = True if val == "True" else False
        if param == "alpha":
            alpha = float(val)
    return MultinomialNB(alpha=alpha, fit_prior=learn_prior, class_prior=None)


def get_logistic_regression(params):
    """
    Instantiates a Logistic regression classifier in scikit.

    Parameters
    ----------
    params: List of parameters to be used for instantiating the classifier.

    Returns
    -------
    Logistic regression classifier from scikit.

    """
    dual = False
    C = 1.0
    seed = 42
    max_iter = 1000
    solver = "liblinear"
    penalty = "l1"
    class_weight = None
    tolerance = 0.001
    multi_class = None

    for param, val in params[0]["classifier"].iteritems():
        if param == "dual":
            dual = True if val == "True" else False
        if param == "c":
            C = float(val)
        if param == "random_state":
            seed = int(val)
        if param == "tol":
            tolerance = float(val)
        if param == "solver":
            solver = val
        if param == "max_iter":
            max_iter = int(val)
        if param == "penalty":
            penalty = val
        if param == "multi_class":
            if val == "None":
                multi_class = None
            else:
                multi_class = val
        if param == "class_weight":
            try:
                class_weight = ast.literal_eval(val)
            except ValueError:
                if val == "auto":
                    class_weight = val
                else:
                    class_weight = None
    print "Logistic regression parameters"
    print "dual", dual
    print "C", C
    print "seed", seed
    print "tolerance", tolerance
    print "solver", solver
    print "max_iter", max_iter
    print "penalty", penalty
    print "multi-class", multi_class, type(multi_class)
    print "class_weight (left out if equal weights)", class_weight, \
        type(class_weight)
    # Equal class weights
    if class_weight is None:
        # No multi-class
        if multi_class is None:
            print "equal class weights, binary problem"
            return LogisticRegression(
                dual=dual,
                C=C,
                random_state=seed,
                tol=tolerance,
                solver=solver,
                max_iter=max_iter,
                penalty=penalty,
            )
        else:
            print "equal class weights, multi-class problem"
            return LogisticRegression(
                dual=dual,
                C=C,
                random_state=seed,
                tol=tolerance,
                solver=solver,
                max_iter=max_iter,
                penalty=penalty,
                multi_class=multi_class
            )
    else:
        # Weighted classes
        # No multi-class
        if multi_class is None:
            print "weighted class weights, binary problem"
            return LogisticRegression(
                dual=dual,
                C=C,
                random_state=seed,
                tol=tolerance,
                solver=solver,
                max_iter=max_iter,
                penalty=penalty,
                class_weight=class_weight
            )
        else:
            print "weighted class weights, multi-class problem"
            return LogisticRegression(
                dual=dual,
                C=C,
                random_state=seed,
                tol=tolerance,
                solver=solver,
                max_iter=max_iter,
                penalty=penalty,
                multi_class=multi_class,
                class_weight=class_weight
            )


def get_svm(params):
    """
    Instantiates an SVM classifier in scikit.

    Parameters
    ----------
    params: List of parameters to be used for instantiating the classifier.

    Returns
    -------
    SVM classifier from scikit.

    """
    C = 1.0
    kernel = "rbf"
    degree = 3
    shrinking = True
    gamma = 0.0
    coeff = 0.0
    seed = 42
    class_weight = None
    tolerance = 0.001
    cache_size = None
    max_iter = -1
    for param, val in params[0]["classifier"].iteritems():
        if param == "c":
            C = float(val)
        if param == "kernel":
            kernel = val
        if param == "degree":
            degree = int(val)
        if param == "shrinking":
            shrinking = True if val == "True" else False
        if param == "gamma":
            gamma = float(val)
        if param == "coef0":
            coeff = float(val)
        if param == "random_state":
            seed = int(val)
        if param == "max_iter":
            max_iter = int(val)
        if param == "tol":
            tolerance = float(val)
        if param == "cache_size":
            cache_size = int(val) if val != "None" else None

    print "C", C
    print "kernel", kernel
    print "degree", degree
    print "shrinking", shrinking
    print "gamma", gamma
    print "coef0", coeff
    print "seed", seed
    print "max_iter", max_iter
    print "tol", tolerance
    print "cache size", cache_size

    if cache_size is None:
        return SVC(
            C=C,
            kernel=kernel,
            degree=degree,
            shrinking=shrinking,
            gamma=gamma,
            coef0=coeff,
            random_state=seed,
            max_iter=max_iter,
            tol=tolerance,
        )
    else:
        return SVC(
            C=C,
            kernel=kernel,
            degree=degree,
            shrinking=shrinking,
            gamma=gamma,
            coef0=coeff,
            random_state=seed,
            max_iter=max_iter,
            tol=tolerance,
            cache_size=cache_size
        )


def get_classifier(params):
    """
    Creates a classifier based on the parameters.

    Parameters
    ----------
    params: List of parameters to be used for instantiating the classifier.

    Returns
    -------
    Classifier based on the parameters. If the specified algorithm isn't
    supported, None is returned.

    """
    algo = params[0]["classifier"]["name"]
    is_multi_class = False
    mc_type = None
    if "multi_class" in params[0]["classifier"] and not algo == \
            "LogisticRegression":
        is_multi_class = True
        print "multi-class dict", params[0]["classifier"]["multi_class"]
        if "type" in params[0]["classifier"]["multi_class"]:
            mc_type = params[0]["classifier"]["multi_class"]["type"]
        else:
            raise ValueError("In the config file multi-class is selected, "
                             "but no type is specified.")
    print "use multi-class?", is_multi_class
    print "type?", mc_type
    clf = None
    if algo == "RandomForest":
        clf = get_random_forest(params)
    if algo == "LogisticRegression":
        clf = get_logistic_regression(params)
    # MNB has no seed
    if algo == "MultiNominalNaiveBayes":
        clf = get_mnb(params)
    if algo == "SVM":
        clf = get_svm(params)
    if algo == "KNearestNeighbor":
        use_knn = True
    if is_multi_class:
        if mc_type == "OneVsOne":
            return OneVsOneClassifier(clf)
        else:
            return OneVsRestClassifier(clf)
    return clf


def evaluate(classifier, tweets, eval_params, pre_params, output_dir):
    """
    Evaluate a given classifier according to the specified parameters.

    Parameters
    ----------
    classifier: Scikit classifier.
    tweets: List of tweet objects representing the dataset.
    eval_params: Parameters from the configuration file to be used for
                 evaluation.
    pre_params: Parameters used for pre-processing.
    output_dir: Directory in which all results should be stored.

    Returns
    -------
    Specified scores/metrics indicating how successful the given model is.

    """
    use_grid_search = False
    use_split = False
    use_cv = False
    # Add all selected scores
    metrics = []
    seed = 42
    precomputed = None
    use_precomputed = False
    is_regression = False
    # Default values for CV
    cv_params = {
        "cv_type": "k",
        "shuffle": True,
        "train_size": 0.8,
        "outer_folds": 10,
        "inner_folds": 10,
    }
    # Default values for splitting data into train and test set
    split_params = {
        "train_size": 0.8,
        "shuffle": True,
    }
    params = eval_params[0]["evaluation"]
    for k, v in params.iteritems():
        # print k, ":", v
        if k == "metrics":
            if "f1" in params[k]:
                metrics.append("f1")
            if "precision" in params[k]:
                metrics.append("precision")
            if "recall" in params[k]:
                metrics.append("recall")
            if "accuracy" in params[k]:
                metrics.append("accuracy")
            if "roc" in params[k]:
                metrics.append("roc"),
            if "confusion_matrix" in params[k]:
                metrics.append("confusion_matrix")
            if "support" in params[k]:
                metrics.append("support")
            # TODO: This is optimization
            if "learning_curve" in params[k]:
                metrics.append("learning_curve")
        if k == "is_regression":
            if v == "True":
                is_regression = True
        if k == "precomputed":
            precomputed = params[k]["base"]
            if params[k]["use"] == "True":
                use_precomputed = True
        if k == "cv":
            use_cv = True
            if "inner_folds" in params[k]:
                cv_params["inner_folds"] = int(params[k]["inner_folds"])
            if "outer_folds" in params[k]:
                cv_params["outer_folds"] = int(params[k]["outer_folds"])
            if "train_size" in params[k]:
                cv_params["train_size"] = float(params[k]["train_size"])
            if "shuffle" in params[k]:
                if params[k]["shuffle"] == "False":
                    cv_params["shuffle"] = False
            if "type" in params[k]:
                cv_params["cv_type"] = params[k]["type"]
        if k == "split":
            use_split = True
            if "train_size" in params[k]:
                split_params["train_size"] = float(params[k]["train_size"])
            if "shuffle" in params[k]:
                split_params["shuffle"] = True if params[k]["shuffle"] == \
                                                 "True" else False
        # TODO: Grid search is also part of optimization -> put it there
        if k == "grid_search":
            use_grid_search = True
        if k == "seed":
            seed = v
    print "regression problem?", is_regression
    print "use CV?", use_cv
    print "store/load dataset in/from", precomputed
    print "use pre-built dataset?", use_precomputed
    print "use grid search?", use_grid_search
    print "use split?", use_split
    print "metrics for evaluation", metrics
    print "seed", seed
    print "cv parameters", cv_params
    print "split parameters", split_params
    # output_dir = out_params[0]["output"]["base"]
    print "Store results under:", output_dir
    # CV has precedence over simple split
    if use_cv and use_split:
        use_split = False
    if use_cv:
        print "use cv"
        for metric in metrics:
            # Run CV
            score = _run_cv(tweets, classifier, metric, pre_params,
                            output_dir, precomputed,
                            use_precomputed=use_precomputed,
                            is_regression=is_regression,
                            seed=seed,
                            **cv_params)
    if use_split:
        print "use split"
        score = _run_split_evaluation(tweets, classifier, metric, pre_params,
                                      output_dir, seed=seed, **split_params)
    if use_grid_search:
        print "use grid search"


def _run_cv(tweets, clf, score, pre_params, output_dir, prebuilt,
            use_precomputed=False, is_regression=True, cv_type="k",
            outer_folds=10, inner_folds=10, train_size=0.8, seed=42,
            shuffle=True):
    """
    Performs CV with the given values. How to calculate ROC and F1 in an
    unbiased manner with CV?
    see "Apples-to-Apples in Cross-Validation Studies: Pitfalls in Classifier
    Performance Measurement" by Forman and Scholz: average ROC over the folds
    and compute F1 at the end based on the final confusion table for all
    classes.

    Parameters
    ----------
    tweets: Dataset to be used. In our case the tweet texts in BoW
            representation.
    clf: Scikit classifier object used in CV.
    score: Score to be used for evaluating the suitability of a given model.
    pre_params: Parameters used for pre-processing.
    output_dir: Base directory in which the results will be stored.
    prebuilt: If not None, the preprocessed (stored) dataset with the given
              name will be used for CV. The user has to make sure that it's
              suitable (data must be available for each fold).
    is_regression: True if labels are numbers. Otherwise it's treated as a
                   classification problem.
    cv_type: String to specify which type of CV to use. Valid values are:
             "shuffle" for random permutations CV, "k" for simple k-fold CV,
             "bootstrap" for bootstrapped CV or
             "stratified" for stratified CV. Default = "k".
    outer_folds: Number of folds in outer loop of nested CV.
    inner_folds: Number of folds in inner loop of nested CV.
    train_size: Amount of data to be used for training. This is only relevant if
                "shuffle" for cv_type is selected. Parameter will be ignored
                otherwise.
    seed: Seed for PRNG to allow reproducibility of results.
    shuffle: Dataset will be shuffled prior to creating folds if True. If
             False, dataset will be split as is.

    """
    # It could happen that some tweets need to be removed -> do this before
    # computing the folds!
    tweets = _remove_invalid_tweets(tweets, pre_params)
    CLASSES = list(set([t.label for t in tweets]))
    print "#CLASSES", CLASSES
    CLASSES = sorted(CLASSES)
    # Location of preprocessed dataset
    PREBUILT_DATASET = "../preprocessed_datasets"
    outer = _get_cv_iterator(len(tweets), cv_type=cv_type, folds=outer_folds,
                             train_size=train_size, seed=seed, shuffle=shuffle)

    # Evaluate multi-class classifiers correctly
    is_multi_class = False
    if isinstance(clf, OneVsRestClassifier):
        is_multi_class = True
    if isinstance(clf, OneVsOneClassifier):
        is_multi_class = True
    # Logistic regression handles multi-class internally, so we need to check
    #  it separately and overwrite the settings
    if isinstance(clf, LogisticRegression):
        if "multi_class" in clf.get_params():
            is_multi_class = True
    print "CV iterator:", type(outer)
    print "classification problem", type(clf)
    outer_scores = {}   # {fold1: {compute_aggregate_score()}, fold2: {...}}
    outer_f1_scores = []
    outer_confusion_matrix = np.zeros((len(CLASSES), len(CLASSES)))
    # outer_scores = []
    # Apply nested CV to get an unbiased estimate of our model: all
    # pre-processing, optimization steps etc. must be applied per fold.
    # Otherwise data leakage might occur yielding an optimistic estimate of a
    #  model's performance, e.g. creating BoW representation on all tweets
    # implies no missing tweets in the test set as all words were already
    # considered. This is highly unrealistic -> build BoW in each inner CV loop

    # outer cross-validation
    for fold, (train_idx_outer, test_idx_outer) in enumerate(outer):
        fold += 1
        print "\nFOLD:", fold
        print "--------"
        # Path under which the preprocessed dataset for this fold will be
        # stored or loaded from
        train_path = os.path.join(PREBUILT_DATASET, prebuilt + str(
            outer_folds) + "_train" +
                                  "_fold_" + str(fold) + ".txt")
        test_path = os.path.join(PREBUILT_DATASET, prebuilt + str(
            outer_folds) + "_test" +
                                 "_fold_" + str(fold) + ".txt")
        # Do preprocessing in here to avoid data leakage
        print "pre_params", pre_params
        # If no preprocessed model should be used
        if not use_precomputed:
            train_outer, t_vectorizer = \
                preprocess_tweets(tweets,
                                  pre_params,
                                  selected_tweets=train_idx_outer
                                  )
            print "Training instances:", len(train_outer)
            print "indices of tweets in test set", train_idx_outer
            # for i in train_idx_outer:
            #     print tweets[i].tweet_id
            print "elements in test:", test_idx_outer.shape[0]
            print "indices of tweets in test set", test_idx_outer
            # for i in test_idx_outer:
            #     print tweets[i].tweet_id
            test_outer, vectorizer = \
                preprocess_tweets(tweets,
                                  pre_params,
                                  selected_tweets=test_idx_outer,
                                  vectorizer=t_vectorizer
                                  )
            # Store existing preprocessed training/test dataset per fold
            with open(os.path.join(PREBUILT_DATASET, prebuilt + str(
                outer_folds) + "_train" + "_fold_" + str(fold) + ".txt"),
                      "wb") as f:
                cPickle.dump(train_outer, f)
            with open(os.path.join(PREBUILT_DATASET, prebuilt + str(
                outer_folds) + "_test" + "_fold_" + str(fold) + ".txt"),
                      "wb") as f:
                cPickle.dump(test_outer, f)
        else:
            print "Load existing dataset"
            # Load existing preprocessed training/test dataset per fold
            with open(train_path, "rb") as f:
                train_outer = cPickle.load(f)
            with open(test_path, "rb") as f:
                test_outer = cPickle.load(f)
        # To make computations more efficient, let's turn the list into a numpy
        # array and separate the X and y explicitly
        x_train = [t.bow for t in train_outer]
        y_train = [t.label for t in train_outer]
        X_train_outer = np.array(x_train)
        y_train_outer = np.array(y_train)
        x_test = [t.bow for t in test_outer]
        y_test = [t.label for t in test_outer]
        X_test_outer = np.array(x_test)
        y_test_outer = np.array(y_test)
        # Important: scale features for SVM and logistic regression!
        # Otherwise they don't work well!
        # In multi-class problems clf is either OVR or OVO classifier ->
        # check it's estimator instead
        if isinstance(clf, OneVsOneClassifier) or \
                isinstance(clf, OneVsRestClassifier):
                if isinstance(clf.get_params()["estimator"], SVC):
                    scaler = StandardScaler()
                    X_train_outer = scaler.fit_transform(
                        X=X_train_outer.astype(np.float))
                    X_test_outer = scaler.transform(X=X_test_outer.astype(
                        np.float))
        # Logistic regression has internal multi-class parameter
        elif isinstance(clf, LogisticRegression):
            scaler = StandardScaler()
            X_train_outer = scaler.fit_transform(X=X_train_outer.astype(
                np.float))
            X_test_outer = scaler.transform(X=X_test_outer.astype(np.float))
        inner_mean_scores = []
        # define explored parameter space.
        # procedure below should be equal to GridSearchCV
        # TODO: let these parameters be set from config file as a dictionary
        # optimize:
        #   name: param1
        #   range: (start, end, step)
        #
        # and then when instantiating the classifier, replace the config
        # parameter with the optimal solution, something like
        # params = old_params
        # params[name] = optimized value
        # clf = get_algorithm(params)
        # tuned_parameter = [1000, 1100, 1200]
        tuned_parameter = [1000]

        # for param in tuned_parameter:
        #
        #     inner_scores = []
        #
        #     # inner cross-validation
        #     # inner = KFold(len(X_train_outer), n_folds=2, shuffle=True,
        #     #               random_state=seed)
        #     inner = _get_cv_iterator(len(X_train_outer), folds=inner_folds,
        #                              train_size=train_size, seed=seed)
        #     for train_index_inner, test_index_inner in inner:
        #         # split the training data of outer CV
        #         X_train_inner, X_test_inner = \
        #             X_train_outer[train_index_inner], X_train_outer[
        #                 test_index_inner]
        #         y_train_inner, y_test_inner = y_train_outer[train_index_inner],\
        #                                       y_train_outer[test_index_inner]
        #
        #         # fit extremely randomized trees regressor to training data of
        #         # inner CV
        #         # TODO: instantiate new classifier
        #         # clf = RandomForestClassifier(n_estimators=100)
        #         # clf = ensemble.ExtraTreesRegressor(param, n_jobs=-1,
        #         # random_state=1)
        #         clf.fit(X_train_inner, y_train_inner)
        #         # TODO: calculate score for metric
        #         inner_scores.append(clf.score(X_test_inner, y_test_inner))
        #
        #     # calculate mean score for inner folds
        #     inner_mean_scores.append(np.mean(inner_scores))
        #
        # # get maximum score index
        # index, value = max(enumerate(inner_mean_scores),
        #                    key=operator.itemgetter(1))
        #
        # print 'Best parameter of %i fold: %i' % (fold + 1, tuned_parameter[
        #     index])

        # fit the selected model to the training set of outer CV
        # for prediction error estimation
        # TODO: initiate classifier with tuned parameters here
        # clf2 = RandomForestClassifier(n_estimators=tuned_parameter[index])
        # clf2 = ensemble.ExtraTreesRegressor(tuned_parameter[index],
        # n_jobs=-1, random_state=1)
        clf.fit(X_train_outer, y_train_outer)
        # Store classifier per fold
        name = "classifier_" + str(fold) + ".pkl"
        joblib.dump(clf, os.path.join(output_dir, name), compress=9)
        # This is how you can load the classifier from the file:
        #clf = joblib.load(os.path.join(output_dir, name))
        pred = clf.predict(X_test_outer)
        print "multi-class?", is_multi_class
        current_score = compute_single_score(
            pred, y_test_outer, score, classes=CLASSES,
            is_regression=is_regression, is_multi_class=is_multi_class)
        inner_f1_score = f1_score(y_test_outer, pred, average="micro")
        outer_f1_scores.append(inner_f1_score)
        cm = confusion_matrix(y_test_outer, pred, labels=np.array(CLASSES))
        print "confusion matrix in fold ", fold, ":"
        print cm
        outer_confusion_matrix += cm
        # outer_confusion_matrix += confusion_matrix(y_test_outer, pred,
        #                                            labels=np.array(CLASSES))

        # outer_scores.append(clf2.score(X_test_outer, y_test_outer))
        outer_scores[fold] = current_score
    CLASSES = np.array(CLASSES)
    _compute_aggregate_score(outer_scores, score, outer_folds, output_dir,
                            is_multi_class=is_multi_class)
    print "order of classes in confusion matrix", np.array(CLASSES)
    print "confusion matrix"
    print outer_confusion_matrix
    f1_cls_scores, avg_f1 = _calculate_f1(outer_confusion_matrix, np.array(
        CLASSES))
    print "Results of biased F1 score:"
    print "f1 scores per fold"
    print outer_f1_scores
    print np.mean(outer_f1_scores)
    print "Results of unbiased F1 score:"
    for cls_idx, cls in enumerate(CLASSES):
        print "Class {}: {}".format(cls, f1_cls_scores[cls_idx])
    print "Weighted average F1: ", avg_f1
    with open(os.path.join(output_dir, "metrics.txt"), "w") as f:
        f.write("F1 score per class:\n")
        for cls_idx, cls in enumerate(CLASSES):
            f.write("Class {}: {}\n".format(cls, f1_cls_scores[cls_idx]))
        f.write("Average weighted F1: {}".format(avg_f1))
    # show the prediction error estimate produced by nested CV
    # print 'Unbiased prediction error: %.2f (+/- %.2f)' % (np.mean(outer_scores),
    #                                            np.std(outer_scores))
    # Build final model now based on all training data because we have
    # estimated its performance above
    data, t_vectorizer = preprocess_tweets(tweets, pre_params)
    X = np.array([t.bow for t in data])
    y = np.array([t.label for t in data])
    clf.fit(X, y)
    joblib.dump(clf, os.path.join(output_dir, "final_classifier.pkl"),
                compress=9)
    joblib.load(os.path.join(output_dir, "final_classifier.pkl"))
    clf.predict(X)


def _calculate_f1(conf_matrix, classes):
    """
    Calculate F1 score with precision and recall based on the confusion
    matrix. This computation is done based on the final confusion matrix for
    binary or multi-class problems.
    F1 score is averaged over the weighted F1 scores per class.

    confusion matrix
    true (y-axis)/predicted (x-axis)
    class    1     2     3     4     5
      1 [[   0.    0.    8.    1.    1.]
      2  [   0.    2.   17.    3.    0.]
      3  [   0.    0.  131.    6.    8.]
      4  [   0.    0.   46.   18.    2.]
      5  [   0.    0.   31.    3.   10.]]

    Parameters
    ----------
    conf_matrix: numpy array [n_classes, n_classes] - symmetric matrix where
    the rows correspond to the ground truth and columns to predicted values.
    classes: numpy array indicating the order of the classes.

    Returns
    -------
    List, float.
    List of F1 scores per class in the same order as <classes>, weighted F1
    score.

    """
    print "dimensions confusion matrix", conf_matrix.shape[0]
    dims = conf_matrix.shape[0]
    TP = {}  # {class1: count, class2: count}
    FP = {}
    FN = {}
    TN = {}
    instances = {}  # = elements in row
    for cls_idx, cls in enumerate(classes):
        for i in range(dims):
            for j in range(dims):
                if i == j and i == cls_idx:
                    TP[cls] = conf_matrix[i][j]
                elif not i == cls_idx and not j == cls_idx:
                    # All other elements having a different i and j are TNs
                    if cls not in TN:
                        TN[cls] = 0
                    TN[cls] += conf_matrix[i][j]
                elif i != j and j == cls_idx:
                    # j-th column represents FPs for class j
                    if cls not in FP:
                        FP[cls] = 0
                    FP[cls] += conf_matrix[i][j]
                elif i != j and i == cls_idx:
                    # i-th row corresponds to FNs of class i
                    if cls not in FN:
                        FN[cls] = 0
                    FN[cls] += conf_matrix[i][j]
                if i == cls_idx:
                    if cls not in instances:
                        instances[cls] = 0
                    instances[cls] += conf_matrix[i][j]
    print "TP", TP
    print "TN", TN
    print "FP", FP
    print "FN", FN
    print "#instances per class", instances
    return _weighted_f1(TP, FP, FN, instances, classes)


def _f1(TP, FP, FN):
    """
    Computes F1 score based on TPs, FPs and FNs.
    """
    return 2. * TP / (2 * TP + FN + FP)


def _weighted_f1(TP, FP, FN, instances, classes):
    """
    Computes the weighted average F1 score given TPs, FPs, FNs and number of
    instances per class.

    Parameters
    ----------
    TP: Dictionary containing true positives per class.
    FP: Dictionary containing false positives per class.
    FN: Dictionary containing false negatives per class.
    instances: Dictionary containing the number of instances per class.
    classes: Classes to be used in the dataset. Order of the classes
    determines order of the F1 scores.

    Returns
    -------
    List, float.
    List of F1 scores per class in the same order as <classes>, weighted F1
    score.

    """
    total_instances = sum(instances.values())
    print "total number of instances", total_instances
    avg_f1 = 0
    f1_scores = []
    for cls in classes:
        cls_weight = 1. * instances[cls] / total_instances
        print "weight of class", cls, ":", cls_weight
        f1 = _f1(TP[cls], FP[cls], FN[cls])
        f1_scores.append(f1)
        avg_f1 += f1 * cls_weight
    return f1_scores, avg_f1


def _run_split_evaluation(tweets, clf, score, pre_params, output_dir,
                          train_size=0.8, seed=42, shuffle=True):
    """
    Runs evaluation by splitting the dataset into a train and test set
    without applying CV.

    Parameters
    ----------
    tweets: Dataset to be used. In our case the tweet texts in BoW
            representation.
    clf: Scikit classifier object used in CV.
    score: Score to be used for evaluating the suitability of a given model.
    pre_params: Parameters used for pre-processing.
    output_dir: Base directory in which the results will be stored.
    train_size: Amount of data to be used for training. This is only relevant if
                "shuffle" for cv_type is selected. Parameter will be ignored
                otherwise.
    seed: Seed for PRNG to allow reproducibility of results.
    shuffle: Dataset will be shuffled prior to creating folds if True. If
             False, dataset will be split as is.

    Returns
    -------

    """
    # metric = _get_score(pred, true, score, is_multi_class=is_multi_class)
    X = [t.bow for t in tweets]
    y = [t.label for t in tweets]
    X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                        train_size=train_size)
    clf.fit(X_train, y_train)
    predicted = clf.predict(X_test)
    # TODO: compute scores here
    score = clf.score(y_test, predicted)
    return score


def _get_cv_iterator(inst, cv_type="k", folds=10,
                     train_size=0.8, seed=42, shuffle=True):
    """
    Creates a CV iterator according to the given parameters.

    Parameters
    ----------
    inst: Number of instances in the dataset.
    cv_type: String to specify which type of CV to use. Valid values are:
             "shuffle" for random permutations CV, "k" for simple k-fold CV,
             "bootstrap" for bootstrapped CV or
             "stratified" for stratified CV. Default = "k".
    folds: Number of folds in CV.
    train_size: Amount of data to be used for training. This is only relevant if
                "shuffle" for cv_type is selected. Parameter will be ignored
                otherwise.
    seed: Seed for PRNG to allow reproducibility of results.
    shuffle: Dataset will be shuffled prior to creating folds if True. If
             False, dataset will be split as is

    Returns
    -------
    CV iterator of the specified type. If an error occurred or some
    parameters were missing, None is returned.

    """
    seed = int(seed)
    if cv_type == "stratified":
        outer = StratifiedKFold(
            inst,
            n_folds=folds,
            shuffle=shuffle,
            random_state=seed,
        )
    elif cv_type == "shuffle":
        test_size = 1 - train_size
        outer = ShuffleSplit(
            inst,
            n_iter=folds,
            train_size=train_size,
            test_size=test_size,
            random_state=seed,
        )
    elif cv_type == "bootstrap":
        test_size = 1 - train_size
        outer = Bootstrap(
            inst,
            n_iter=folds,
            train_size=train_size,
            test_size=test_size,
            random_state=seed,
        )
    else:
        outer = KFold(
            inst,
            shuffle=shuffle,
            n_folds=folds,
            random_state=seed,
        )
    return outer


def compute_single_score(pred, true, metric, classes=[-1, 1],
                         is_regression=True, is_multi_class=False):
    """
    Computes how well a model performed in predicting labels for a single fold.

    Parameters
    ----------
    pred: List of predicted labels.
    true: List of true labels.
    metric: Name of a metric to compute score.
    classes: List of classes in the dataset.
    is_regression: True if labels are numbers. Otherwise it's treated as a
                   classification problem.
    is_multi_class: If True, score is computed fo multi-class problem and not
                    a binary one.

    Returns
    -------
    Score given the predictions.

    """
    if metric == "roc":
        print "!!!!!!!!!!!!!!!!!compute score for ROC"
        return _compute_roc_auc(pred, true, classes=classes,
                                is_regression=is_regression,
                                is_multi_class=is_multi_class)
    return None


def _compute_roc_auc_regression(pred, true, classes=[-1, 1],
                              is_multi_class=False):
    """
    Computes ROC if the 3 classes 'positive', 'neutral' and 'negative' are
    given as a regression problem.

    Parameters
    ----------
    pred: List of predicted labels.
    true: List of true labels.
    classes: List of classes in dataset.
    is_multi_class: If True, score is computed fo multi-class problem and not
                    a binary one.

    Returns
    -------
    dict.
    Dictionary containing the results per class: once for "default"
    computation, once for "micro" aggregation. For both the FPR, FNR and AUC
    are stored as separate entries, e.g.
    {"default":
        {"positive":
            { "fpr": 1,
              "tpr": 0.4,
              "auc": 0.6,
            },
        "negative":
            { "fpr": 1,
              "tpr": 0.4,
              "auc": 0.6,
            }
        },
        "micro":
            { "fpr": 0.5,
              "tpr": 0.5,
              "auc": 0.7,
            }
        ,
        "negative": ...
    }

    """
    if is_multi_class:
        print "calculate ROC multi regression"
        # http://scikit-learn.org/stable/auto_examples/model_selection/
        # plot_roc.html
        # Compute ROC curve and ROC area for each class
        result = {
            "micro": {},
            "default": {}
        }
        # Make sure that in binarization the mapping of the labels is correct
        #  to map the right results to each class
        # map_str2i = {"vneg": "1", "pneg": "2", "negative": "3", "neutral": "4",
        #              "positive": "5"}
        # map_i2str = {"1": "vneg", "2": "pneg", "3": "negative",
        #              "4": "neutral", "5": "positive"}
        map_str2i = {}
        map_i2str = {}
        labels = set(true)
        labels = sorted(list(labels))
        print labels
        for no, l in enumerate(labels):
            map_str2i[str(l)] = str(no + 1)
            map_i2str[str(no + 1)] = str(l)
        print "Truth", true
        print "str2i", map_str2i
        print "i2str", map_i2str
        # labels = [l for l in map_str2i.iterkeys()]
        print "labels", labels
        true = label_binarize(true, classes=labels)
        # print "Truth binarized", true
        pred = label_binarize(pred, classes=labels)
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        # true = np.array(true)
        # pred = np.array(pred)
        print "classes", classes
        # TODO: can <classes> be removed???
        for i, cls in enumerate(labels):
            print type(cls), cls, cls in map_i2str[cls]
            result["default"][map_i2str[cls]] = {}
            result["default"][map_i2str[cls]]["fpr"], result["default"][
                map_i2str[cls]]["tpr"], _ = roc_curve(true[:, i], pred[:, i])
            result["default"][map_i2str[cls]]["auc"] = \
                auc(result["default"][map_i2str[cls]]["fpr"], result[
                    "default"][map_i2str[cls]]["tpr"])
        # Compute micro-averaged ROC curve and ROC area
        # fpr["micro"], tpr["micro"], _ = roc_curve(true.ravel(), pred.ravel())
        # roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        # result["micro"] = {}
        # result["micro"]["fpr"], result["micro"]["tpr"], _ = roc_curve(
        #     true.ravel(), pred.ravel())
        # result["micro"]["auc"] = auc(result["micro"]["fpr"], result["micro"][
        #     "tpr"])
        print "fpr", fpr
        print "tpr", tpr
        print "auc", roc_auc
        return result
    else:
        print "calculate ROC binary"
        print pred
        # http://scikit-learn.org/stable/auto_examples/model_selection/
        # plot_roc.html
        # Compute ROC curve and ROC area for each class
        result = {
            "micro": {},
            "default": {}
        }
        # Make sure that in binarization the mapping of the labels is correct
        #  to map the right results to each class
        existing_labels = sorted(classes)
        print "labels", existing_labels
        print "largest one", existing_labels[-1]
        print "Truth", true
        true = np.array(true)
        pred = np.array(pred)
        print "classes", classes
        print "true", true.shape
        print "pred", pred.shape
        result["default"] = {}
        print "shape", true.shape, pred.shape, type(existing_labels[-1]),
        # Choose the largest label as positive class
        result["default"]["fpr"], result["default"]["tpr"], _ = roc_curve(
            true, pred, pos_label=int(existing_labels[-1]))
        result["default"]["auc"] = \
            auc(result["default"]["fpr"], result["default"]["tpr"])
        # Compute micro-averaged ROC curve and ROC area
        # fpr["micro"], tpr["micro"], _ = roc_curve(true.ravel(), pred.ravel())
        # roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        # result["micro"] = {}
        # result["micro"]["fpr"], result["micro"]["tpr"], _ = roc_curve(
        #     true.ravel(), pred.ravel(), pos_label=int(existing_labels[-1]))
        # result["micro"]["auc"] = auc(result["micro"]["fpr"], result["micro"][
        #     "tpr"])
        # print "fpr", fpr
        # print "tpr", tpr
        # print "auc", roc_auc
        return result


def _compute_roc_auc_classification(pred, true, classes=[-1, 1],
                                    is_multi_class=False):
    """
    Computes ROC.

    Parameters
    ----------
    pred: List of predicted labels.
    true: List of true labels.
    classes: List of classes in dataset.
    is_multi_class: If True, score is computed fo multi-class problem and not
                    a binary one.

    Returns
    -------
    dict.
    Dictionary containing the results per class: once for "default"
    computation, once for "micro" aggregation. For both the FPR, FNR and AUC
    are stored as separate entries, e.g.
    {"default":
        {"positive":
            { "fpr": 1,
              "tpr": 0.4,
              "auc": 0.6,
            },
        "negative":
            { "fpr": 1,
              "tpr": 0.4,
              "auc": 0.6,
            }
        },
        "micro":
            { "fpr": 0.5,
              "tpr": 0.5,
              "auc": 0.7,
            }
        ,
        "negative": ...
    }

    """
    if is_multi_class:
        print "calculate ROC multi classification"
        # http://scikit-learn.org/stable/auto_examples/model_selection/
        # plot_roc.html
        # Compute ROC curve and ROC area for each class
        result = {
            "micro": {},
            "default": {}
        }
        # Make sure that in binarization the mapping of the labels is correct
        #  to map the right results to each class
        # map_str2i = {"vneg": "1", "pneg": "2", "negative": "3", "neutral": "4",
        #              "positive": "5"}
        # map_i2str = {"1": "vneg", "2": "pneg", "3": "negative",
        #              "4": "neutral", "5": "positive"}
        map_str2i = {}
        map_i2str = {}
        labels = set(true)
        labels = sorted(list(labels))
        print labels
        for no, l in enumerate(labels):
            map_str2i[str(l)] = str(no + 1)
            map_i2str[str(no + 1)] = str(l)
        print "Truth", true
        print "str2i", map_str2i
        print "i2str", map_i2str
        # labels = [l for l in map_str2i.iterkeys()]
        print "labels", labels
        true = label_binarize(true, classes=labels)
        # print "Truth binarized", true
        pred = label_binarize(pred, classes=labels)
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        # true = np.array(true)
        # pred = np.array(pred)
        print "classes", classes
        # TODO: can <classes> be removed???
        for i, cls in enumerate(labels):
            print type(cls), cls, cls in map_str2i[cls]
            result["default"][map_str2i[cls]] = {}
            result["default"][map_str2i[cls]]["fpr"], result["default"][
                map_str2i[cls]]["tpr"], _ = roc_curve(true[:, i], pred[:, i])
            result["default"][map_str2i[cls]]["auc"] = \
                auc(result["default"][map_str2i[cls]]["fpr"], result[
                    "default"][map_str2i[cls]]["tpr"])
        # Compute micro-averaged ROC curve and ROC area
        # fpr["micro"], tpr["micro"], _ = roc_curve(true.ravel(), pred.ravel())
        # roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        # result["micro"] = {}
        # result["micro"]["fpr"], result["micro"]["tpr"], _ = roc_curve(
        #     true.ravel(), pred.ravel())
        # result["micro"]["auc"] = auc(result["micro"]["fpr"], result["micro"][
        #     "tpr"])
        print "fpr", fpr
        print "tpr", tpr
        print "auc", roc_auc
        return result
    else:
        print "calculate ROC binary"
        print pred
        # http://scikit-learn.org/stable/auto_examples/model_selection/
        # plot_roc.html
        # Compute ROC curve and ROC area for each class
        result = {
            "micro": {},
            "default": {}
        }
        # Make sure that in binarization the mapping of the labels is correct
        #  to map the right results to each class
        existing_labels = sorted(classes)
        print "labels", existing_labels
        print "largest one", existing_labels[-1]
        print "Truth", true
        true = np.array(true)
        pred = np.array(pred)
        print "classes", classes
        print "true", true.shape
        print "pred", pred.shape
        result["default"] = {}
        print "shape", true.shape, pred.shape, type(existing_labels[-1]),
        # Choose the largest label as positive class
        result["default"]["fpr"], result["default"]["tpr"], _ = roc_curve(
            true, pred, pos_label=int(existing_labels[-1]))
        result["default"]["auc"] = \
            auc(result["default"]["fpr"], result["default"]["tpr"])
        # Compute micro-averaged ROC curve and ROC area
        # fpr["micro"], tpr["micro"], _ = roc_curve(true.ravel(), pred.ravel())
        # roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        # result["micro"] = {}
        # result["micro"]["fpr"], result["micro"]["tpr"], _ = roc_curve(
        #     true.ravel(), pred.ravel(), pos_label=int(existing_labels[-1]))
        # result["micro"]["auc"] = auc(result["micro"]["fpr"], result["micro"][
        #     "tpr"])
        # print "fpr", fpr
        # print "tpr", tpr
        # print "auc", roc_auc
        return result


def _compute_roc_auc(pred, true, classes=[-1, 1],
                     is_regression=True, is_multi_class=False):
    """
    Computes ROC.

    Parameters
    ----------
    pred: List of predicted labels.
    true: List of true labels.
    classes: List of classes in dataset.
    is_regression: True if labels are numbers. Otherwise it's treated as a
                   classification problem.
    is_multi_class: If True, score is computed fo multi-class problem and not
                    a binary one.

    Returns
    -------
    dict.
    Dictionary containing the results per class: once for "default"
    computation, once for "micro" aggregation. For both the FPR, FNR and AUC
    are stored as separate entries, e.g.
    {"default":
        {"positive":
            { "fpr": 1,
              "tpr": 0.4,
              "auc": 0.6,
            },
        "negative":
            { "fpr": 1,
              "tpr": 0.4,
              "auc": 0.6,
            }
        },
        "micro":
            { "fpr": 0.5,
              "tpr": 0.5,
              "auc": 0.7,
            }
        ,
        "negative": ...
    }

    """
    # if is_multi_class:
    #     print "calculate ROC multi"
    #     # http://scikit-learn.org/stable/auto_examples/model_selection/
    #     # plot_roc.html
    #     # Compute ROC curve and ROC area for each class
    #     result = {
    #         "micro": {},
    #         "default": {}
    #     }
    #     # Make sure that in binarization the mapping of the labels is correct
    #     #  to map the right results to each class
    #     # map_str2i = {"vneg": "1", "pneg": "2", "negative": "3", "neutral": "4",
    #     #              "positive": "5"}
    #     # map_i2str = {"1": "vneg", "2": "pneg", "3": "negative",
    #     #              "4": "neutral", "5": "positive"}
    if is_regression:
        return _compute_roc_auc_regression(pred, true, classes,
                                         is_multi_class)
    else:
        return _compute_roc_auc_classification(pred, true, classes,
                                         is_multi_class)
    #     map_str2i = {}
    #     map_i2str = {}
    #     labels = set(true)
    #     labels = sorted(list(labels))
    #     print labels
    #     for no, l in enumerate(labels):
    #         map_str2i[str(l)] = str(no + 1)
    #         map_i2str[str(no + 1)] = str(l)
    #     print "Truth", true
    #     print "str2i", map_str2i
    #     print "i2str", map_i2str
    #     # labels = [l for l in map_str2i.iterkeys()]
    #     print "labels", labels
    #     true = label_binarize(true, classes=labels)
    #     # print "Truth binarized", true
    #     pred = label_binarize(pred, classes=labels)
    #     fpr = dict()
    #     tpr = dict()
    #     roc_auc = dict()
    #     # true = np.array(true)
    #     # pred = np.array(pred)
    #     print "classes", classes
    #     # TODO: can <classes> be removed???
    #     for i, cls in enumerate(labels):
    #         print type(cls), cls, cls in map_i2str[cls]
    #         result["default"][map_i2str[cls]] = {}
    #         result["default"][map_i2str[cls]]["fpr"], result["default"][
    #             map_i2str[cls]]["tpr"], _ = roc_curve(true[:, i], pred[:, i])
    #         result["default"][map_i2str[cls]]["auc"] = \
    #             auc(result["default"][map_i2str[cls]]["fpr"], result[
    #                 "default"][map_i2str[cls]]["tpr"])
    #     # Compute micro-averaged ROC curve and ROC area
    #     # fpr["micro"], tpr["micro"], _ = roc_curve(true.ravel(), pred.ravel())
    #     # roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
    #     # result["micro"] = {}
    #     # result["micro"]["fpr"], result["micro"]["tpr"], _ = roc_curve(
    #     #     true.ravel(), pred.ravel())
    #     # result["micro"]["auc"] = auc(result["micro"]["fpr"], result["micro"][
    #     #     "tpr"])
    #     print "fpr", fpr
    #     print "tpr", tpr
    #     print "auc", roc_auc
    #     return result
    # else:
    #     print "calculate ROC binary"
    #     print pred
    #     # http://scikit-learn.org/stable/auto_examples/model_selection/
    #     # plot_roc.html
    #     # Compute ROC curve and ROC area for each class
    #     result = {
    #         "micro": {},
    #         "default": {}
    #     }
    #     # Make sure that in binarization the mapping of the labels is correct
    #     #  to map the right results to each class
    #     existing_labels = sorted(classes)
    #     print "labels", existing_labels
    #     print "largest one", existing_labels[-1]
    #     print "Truth", true
    #     true = np.array(true)
    #     pred = np.array(pred)
    #     print "classes", classes
    #     print "true", true.shape
    #     print "pred", pred.shape
    #     result["default"] = {}
    #     print "shape", true.shape, pred.shape, type(existing_labels[-1]),
    #     # Choose the largest label as positive class
    #     result["default"]["fpr"], result["default"]["tpr"], _ = roc_curve(
    #         true, pred, pos_label=int(existing_labels[-1]))
    #     result["default"]["auc"] = \
    #         auc(result["default"]["fpr"], result["default"]["tpr"])
    #     # Compute micro-averaged ROC curve and ROC area
    #     # fpr["micro"], tpr["micro"], _ = roc_curve(true.ravel(), pred.ravel())
    #     # roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
    #     # result["micro"] = {}
    #     # result["micro"]["fpr"], result["micro"]["tpr"], _ = roc_curve(
    #     #     true.ravel(), pred.ravel(), pos_label=int(existing_labels[-1]))
    #     # result["micro"]["auc"] = auc(result["micro"]["fpr"], result["micro"][
    #     #     "tpr"])
    #     # print "fpr", fpr
    #     # print "tpr", tpr
    #     # print "auc", roc_auc
    #     return result


def _compute_aggregate_score(data, metric, folds, output_dir,
                             is_multi_class=False):
    """

    Computes the overall score of a classifier.

    Parameters
    ----------
    data: Dictionary containing the scores for the seperate folds.
    metric: Name of a metric to compute score.
    folds: Number of folds that were used.
    output_dir: Directory in which the results should be stored.
    is_multi_class: If True, score is computed fo multi-class problem and not
                    a binary one.

    Returns
    -------
    An aggregate score.

    """
    # Display mean ROC curve per class -> aggregate folds per class
    if metric == "roc":
        return _compute_aggregate_roc(data, folds, output_dir,
                                      is_multi_class=is_multi_class)


def _compute_aggregate_roc(data, folds, output_dir, is_multi_class=False):
    """
    Aggregates the results of the separate folds.

    Parameters
    ----------
    data: List of dictionaries containing the scores for the separate folds.
    metric: Name of a metric to compute score.
    folds: Number of folds that were used.
    output_dir: Directory in which the results should be stored.
    is_multi_class: If True, score is computed fo multi-class problem and not
                    a binary one.

    Returns
    -------
    An AUC value for each class and the corresponding TPR and FPR for the ROC
    curve.

    Raises
    ------
    ValueError if no value for one of the classes is available.

    """
    # http://scikit-learn.org/stable/auto_examples/model_selection/plot_roc_crossval.html
    # Calculate a value per class
    mean_tpr = {}
    mean_fpr = {}
    tpr = {}    # {class1: [res of fold1, res of fold2...]}
    fpr = {}
    roc_auc = {}
    if is_multi_class:
        print "multi-class data", data
        for fold in data:
            print "fold", fold
            for cls in data[fold]["default"]:
                # Initialize the values
                if cls not in tpr:
                    tpr[cls] = []
                    fpr[cls] = []
                    mean_tpr[cls] = 0.0
                    mean_fpr[cls] = np.linspace(0, 1, 100)
                    roc_auc[cls] = []
                tmp_tpr = data[fold]["default"][cls]["tpr"]
                tmp_fpr = data[fold]["default"][cls]["fpr"]
                fpr[cls].append(data[fold]["default"][cls]["fpr"])
                mean_tpr[cls] += np.interp(mean_fpr[cls], tmp_fpr, tmp_tpr)
                mean_tpr[cls][0] = 0.0
                tpr[cls].append(data[fold]["default"][cls]["tpr"])
                roc_auc[cls].append(data[fold]["default"][cls]["auc"])
        # print "TPR:", tpr
        # print "FPR:", fpr
        # print "AUC:", auc
        # print "Average TPR", mean_tpr
        # print "Average FPR", mean_fpr
        # print "Average AUC", mean_auc
        for cls in mean_tpr:
            mean_tpr[cls] /= folds
            # At the end it has to be 1
            mean_tpr[cls][-1] = 1.0
            # print type(mean_fpr[cls]), mean_fpr[cls]
            # print type(mean_tpr[cls]), mean_tpr[cls]
            mean_auc = auc(mean_fpr[cls], mean_tpr[cls])
            if np.isnan(mean_auc):
                raise ValueError("No instance available for class %s" % cls)
            plt.plot(mean_fpr[cls], mean_tpr[cls], lw=1,
                     label='Avg. ROC (area = %0.2f) ("%s")' % (
                         mean_auc, cls))
    else:
        tpr = []
        fpr = []
        mean_tpr = 0.0
        mean_fpr = np.linspace(0, 1, 100)
        print "binary data", data
        for fold in data:
            print "fold", fold
            tmp_tpr = fold["default"]["tpr"]
            tmp_fpr = fold["default"]["fpr"]
            fpr.append(fold["default"]["fpr"])
            mean_tpr += np.interp(mean_fpr, tmp_fpr, tmp_tpr)
            mean_tpr[0] = 0.0
            tpr.append(fold["default"]["tpr"])
        print "TPR:", tpr
        print "FPR:", fpr
        print "AUC:", auc
        # print "Average TPR", mean_tpr
        # print "Average FPR", mean_fpr
        # print "Average AUC", mean_auc
        # for cls in mean_tpr:
        mean_tpr /= folds
        # At the end it has to be 1
        mean_tpr[-1] = 1.0
        # print type(mean_fpr[cls]), mean_fpr[cls]
        # print type(mean_tpr[cls]), mean_tpr[cls]
        mean_auc = auc(mean_fpr, mean_tpr)
        if np.isnan(mean_auc):
            raise ValueError("No instance available for binary class")
        plt.plot(mean_fpr, mean_tpr, lw=1,
                 label='Avg. ROC (area = %0.2f)' % (
                     mean_auc))
    # Now do the plotting
    plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Average ROC curve for %s-fold CV' % folds)
    plt.legend(loc="lower right")
    # Store average FPR, TPR and AUC of model
    with open(os.path.join(output_dir, "results.bin"), "wb") as f:
        cPickle.dump(mean_fpr, f)
        cPickle.dump(mean_tpr, f)
        cPickle.dump(mean_auc, f)
    # How to reload
    # with open(os.path.join(output_dir, "results.bin"), "rb") as f:
    #     a = cPickle.load(f)
    #     b = cPickle.load(f)
    #     c = cPickle.load(f)
    # Store ROC curve
    output_dir = os.path.join(output_dir, "plots")
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    plt.savefig(os.path.join(output_dir, "roc.png"))
    return mean_auc


def run_experiments(base_config_dir):
    """
    Executes all specified experiments for a task and stores the outcomes in
    the respective output directories.

    Parameters
    ----------
    base_config_dir: Base directory in which all configuration files for the
    experiments are stored.

    """
    # This is a list of entries each config file MUST include, otherwise it
    # can't be read in successfully and the experiment is bound to fail.
    MANDATORY = ["input", "preprocess", "classifier", "output"]
    # For each config file
    for conf in os.listdir(base_config_dir):
        config_path = os.path.join(base_config_dir, conf)
        # Omit directories
        if os.path.isfile(config_path):
            # print conf
            # Read in the parameters
            in_params, pre_params, train_params, eval_params, out_params = \
                read_config(config_path)
            print "\n------------Reading in parameters completed-------------\n"
            output_dir = out_params[0]["output"]["base"]
            algo = train_params[0]["classifier"]["name"]
            now = datetime.datetime.now()
            now = str(now).split(".")[0]
            name = str(now) + "_" + algo
            output_dir = os.path.join(output_dir, name)
            # Store the config file in the experiment subfolder for
            # reproducibility
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)
            shutil.copy(config_path, os.path.join(output_dir, "config.txt"))
            # Make sure that a directory for the tweet texts of the dataset
            # was specified because otherwise BoW transformation won't work
            # Second, utilize the tweet information
            tweet_path = in_params[0]["input"].get("tweets", None)
            # First, read in the raw directory
            clean_path = in_params[0]["input"]["raw"]
            print "clean path", clean_path
            print "tweet path", tweet_path
            tweets = open_dataset(tweet_path)
            # raw_tweets, tweets_idx = open_cleansed_tsv(clean_path)
            # Add the actual text message to the existing tweets
            # tweets = open_tweets_tsv(
            #     raw_tweets,
            #     tweets_idx,
            #     tweet_path
            # )
            print "Read in: {} tweets for subtask a".format(len(tweets))
            # print "Existing lookup indices: {}".format(len(tweets_idx))
            # tweets = preprocess_tweets(
            #     tweets,
            #     pre_params,
            # )
            print "Entire corpus to be used for training/testing comprises {" \
                  "} tweets.".format(len(tweets))
            # for t in tweets:
            #     print t.tweet_id, t.text
            print "\n----------------Pre-processing completed----------------\n"
            classifier = get_classifier(train_params)
            print "\n----------------Classifier instantiated-----------------\n"
            print "classifier to be used is", type(classifier)
            evaluate(classifier, tweets, eval_params,
                     pre_params, output_dir)
            print "\n-----------------Evaluation completed-------------------\n"


if __name__ == "__main__":
    if settings.DEBUG:
        run_experiments("configs/test")
    else:
        run_experiments("configs")
    # Relative path to the file containing all experiments to be run
