import numpy as np
from scipy import interp
import matplotlib.pyplot as plt

from sklearn import svm, datasets
from sklearn.metrics import roc_curve, auc
from sklearn.cross_validation import KFold
# Test to verify ROC curves and AUC obtained from cross-validation are correct.
# This test only covers the binary case though.
# Based on: http://scikit-learn.org/stable/auto_examples/model_selection
# /plot_roc_crossval.html
from Data_task_4.experiments.run_experiments import compute_single_score, \
    _compute_aggregate_score


def roc_test_gold(X, y, seed=42, folds=6):
    """
    Yields correct ROC curves with CV.
    """
    # Run classifier with cross-validation and plot ROC curves
    cv = KFold(len(y), n_folds=folds, shuffle=True, random_state=42)
    classifier = svm.SVC(kernel='linear', probability=True,
                         random_state=seed)
    mean_tpr = 0.0
    mean_fpr = np.linspace(0, 1, 100)
    for i, (train, test) in enumerate(cv):
        probas_ = classifier.fit(X[train], y[train]).predict_proba(X[test])
        print "probas", probas_.shape
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y[test], probas_[:, 1])
        print "TPR gold in fold {}: {}".format(i, tpr)
        mean_tpr += interp(mean_fpr, fpr, tpr)
        mean_tpr[0] = 0.0
        roc_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, lw=1, label='ROC fold %d (area = %0.2f)' % (i, roc_auc))

    plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')

    mean_tpr /= len(cv)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    plt.plot(mean_fpr, mean_tpr, 'k--',
             label='Mean ROC (area = %0.2f)' % mean_auc, lw=2)

    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show()
    return mean_auc


def my_roc(X, y, seed=42, folds=6):
    """
    Uses our methods to compute AUC.
    """

     # Run classifier with cross-validation and plot ROC curves
    cv = KFold(len(y), n_folds=folds, shuffle=True, random_state=42)
    classifier = svm.SVC(kernel='linear', probability=True,
                         random_state=seed)
    outer_scores = {}
    for i, (train, test) in enumerate(cv):
        probas_ = classifier.fit(X[train], y[train]).predict_proba(X[test])
        single_score = compute_single_score(
            probas_[:, 1], y[test], "roc", classes=["0", "1"], is_multi_class=False)
        outer_scores[i] = single_score
    order = sorted(list(outer_scores))
    print "outer sorted", order
    outer_scores = [outer_scores[fold] for fold in order]
    return _compute_aggregate_score(outer_scores, "roc", folds=folds,
                              is_multi_class=False)


if __name__ == "__main__":
    FOLDS = 6
    SEED = 42
    iris = datasets.load_iris()
    X = iris.data
    y = iris.target
    X, y = X[y != 2], y[y != 2]
    n_samples, n_features = X.shape
    # Add noisy features
    random_state = np.random.RandomState(0)
    X = np.c_[X, random_state.randn(n_samples, 200 * n_features)]
    my_auc = my_roc(X, y, seed=SEED, folds=FOLDS)
    gold_auc = roc_test_gold(X, y, seed=SEED, folds=FOLDS)
    # Compare mean AUCs
    print my_auc, gold_auc, my_auc == gold_auc
    assert(my_auc == gold_auc)
