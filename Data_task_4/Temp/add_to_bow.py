"""
Add a feature manually to BoW representation
"""
from Data_task_4.models.BoW import BoW
from Data_task_4.models.containers import Tweet


if __name__ == "__main__":
    tweet1 = u"I love you very much"
    tweet2 = "But I hate you very much"
    tweet3 = "love hate more"
    ######################
    # Use own vectorizer #
    ######################
    bow = BoW(use_bigrams=True, additional_attrs=["pos", "neg"], min_length=2)
    # Instantiate Tweet objects
    t1 = Tweet(1)
    t1.preprocessed = tweet1
    t1.pos = 0.4
    t1.neg = 0.5
    t1.obj = 0.6
    t2 = Tweet(2)
    t2.preprocessed = tweet2
    t2.pos = 0.3
    t2.neg = 0.3
    t2.obj = 1
    t3 = Tweet(3)
    t3.preprocessed = tweet3
    t3.pos = 0.8
    t3.neg = 0.2
    t3.obj = 0.1
    t4 = Tweet(4)
    t4.preprocessed = ""
    t4.pos = 0.4
    t4.neg = 0.5
    t4.obj = 0.6
    # Build an enriched BoW model
    # a = bow.fit([t1, t2])
    # print "feature names", bow.get_feature_names()
    # print a
    # transformed = bow.transform([t3])
    # print transformed
    # Empty tweet text -> use only additional features for BoW
    a = bow.fit([t1])
    print "vocabulary", bow.vocabulary
    print "feature names", bow.get_feature_names()
    print a
    transformed = bow.transform([t4])
    print "transformed", transformed
    print transformed.toarray()

    #########################
    # Use scikit vectorizer #
    #########################
    # vectorizer = CountVectorizer(tweet1)
    # bow = vectorizer.fit_transform([tweet1, tweet2])
    # # Result
    # # {u'love': 1, u'you': 2, u'hate': 0}
    # # Numbers correspond to indices
    # print vectorizer.vocabulary_
    # print bow
    # transformed = vectorizer.transform([tweet3])
    # # Result:
    # # (0, 0)	1
    # # (0, 1)	1
    # # Interpretation: in row 0, word 0 occurs, which is "hate"; Then word 1
    # # occurs in row 0, which is "love"
    # # Each row corresponds to a single instance
    # print transformed
    # # Now work with the dense representation
    # bow = transformed.toarray()
    # print bow
    # print bow.shape
    # # Add 4 new columns
    # new = np.zeros((bow.shape[0], 4), dtype=np.double)
    # a = np.append(bow, new, axis=1)
    # print "merged", a.shape
    # print a
