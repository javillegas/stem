"""
This file contains the different data types we're using throughout our
repository.
"""


class Tweet(object):
    def __init__(self, t_id):
        """

        Parameters
        ----------
        t_id: Tweet ID.
        topic: Topic about the author was talking.
        label: Label of tweet.
        u_id: ID of tweet author.

        """
        self.tweet_id = t_id


class Option(object):
    pass

