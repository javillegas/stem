import csv
import codecs

from Data_task_4.models.containers import Tweet


def open_cleansed_tsv(rel_path):
    """
    Opens a tsv file. It's assumed it's a cleansed tsv file.

    Parameters
    ---------
    rel_path: Relative path of file.

    Returns
    -------
    List, dict.
    List of tweets in the original tsv file. Dictionary containing the tweet
    IDs as keys and the position in the list as value for quick lookup.

    """
    tweets = []
    idx = {}
    with open(rel_path, "rb") as tsvin:
        tsvin = csv.reader(tsvin, delimiter="\t")
        for pos, row in enumerate(tsvin):
            tweet_id, topic, label = row[0], row[1], row[2]
            t = Tweet(tweet_id, topic, label)
            tweets.append(t)
            idx[tweet_id] = pos
    return tweets, idx


def open_tweets_tsv(tweets, tweets_idx, rel_path):
    """
    Opens a tsv file. It's assumed it's a tsv file describing tweets.

    Parameters
    ---------
    tweets: List of Tweet objects.
    tweets_idx: Dictionary containing for each tweet ID the position at which a
                tweet is stored in <tweets>.
    rel_path: Relative path of file.

    Returns
    -------
    List.
    List of tweets enriched with the information from the tsv file. The tweet
    message is converted to lower case.

    Raises
    ------
    IOError in case rel_path is None.

    """
    valid_tweets = []
    if rel_path is None:
        raise IOError("The specified path (%s) doesn't contain a tweet "
                      "dataset." % rel_path)

    with open(rel_path, "rb") as tsvin:
        tsvin = csv.reader(tsvin, delimiter="\t")
        for row in tsvin:
            tweet_id = row[0]
            pos = tweets_idx[tweet_id]
            tweet_ = tweets[pos]
            tweet_.topic = row[1]
            text = row[3]
            # Convert text to lowercase
            try:
                text = text.lower()
            except UnicodeDecodeError as e:
                text = text.decode('utf-8').lower()
            # Consider all tweets regardless of containing errors or not
            tweet_.text = text
            valid_tweets.append(tweet_)
    return valid_tweets


def open_dataset(rel_path):
    """
    Opens a dataset. It's assumed each line represents a tweet. A tweet might
    or might not contain a column for labels. Tweets for which messages
    couldn't be downloaded are discarded.

    Parameters
    ---------
    rel_path: Relative path of dataset.

    Returns
    -------
    List.
    List of Tweet objects.

    Raises
    ------
    IOError in case rel_path is None.
    ValueError if dataset is in the wrong format.

    """
    tweets = []
    counts = {"positive": 0, "neutral": 0, "negative": 0}
    is_test_set = False
    with open(rel_path, "r") as f:
        content = csv.reader(f, delimiter="\t")
        # Read first line and count columns
        ncol = len(next(content))
        # Go back to beginning of file
        f.seek(0)
        for pos, row in enumerate(content):
            tweet_id = row[0]
            # print "row", tweet_id
            t = Tweet(tweet_id)
            if ncol == 2:
                text = row[1]
                t.text = text
            elif ncol == 3:
                label, text = row[1], row[2]
                if tweet_id == "12541" or tweet_id == "12543":
                    print "tweet text correct? '{}'".format(text)
                # If it's the test set, skip the label
                if label != "UNKNOWN":
                    t.label = label
                else:
                    # Convert UTF-8 manually into respective character
                    text = text.replace("\u002c", ",")
                    text = text.replace("\u2019", "'")
                    text = text.replace('\\"\"', '"')
                    is_test_set = True
                    if "\u" in text:
                        print text
                t.text = text
            else:
                raise ValueError("Dataset has wrong format for task 4a")
            # Consider only tweets for which the messages could be retrieved
            if text != "Not Available":
                if ncol == 3 and not is_test_set:
                    counts[label] += 1
                tweets.append(t)
    print "Label distribution:", counts
    return tweets


def open_dataset2(rel_path):
    """
    Opens a dataset. It's assumed each line represents a tweet. A tweet might
    or might not contain a column for labels. Tweets for which messages
    couldn't be downloaded are discarded.

    Parameters
    ---------
    rel_path: Relative path of dataset.

    Returns
    -------
    List.
    List of Tweet objects.

    Raises
    ------
    IOError in case rel_path is None.
    ValueError if dataset is in the wrong format.

    """
    tweets = []
    counts = {"positive": 0, "neutral": 0, "negative": 0}
    is_test_set = False
    with open(rel_path, "r") as f:
        for pos, line in enumerate(f.readlines()):
            ncol = len(line.split("\t"))
            # Go back to beginning of file
            # f.seek(0)
            # for pos, row in enumerate(line):
            tweet_id = line.split("\t")[0]
            # print "row", tweet_id
            t = Tweet(tweet_id)
            if ncol == 2:
                # text = line.split("\t")[1]
                text = line.split("\t")[1].strip()
                t.text = text
            elif ncol == 3:
                label, text = line.split("\t")[1:3]
                text = text.strip()
                t.text = text
                # If it's the test set, skip the label
                if label != "UNKNOWN":
                    t.label = label
                else:
                    # Convert UTF-8 manually into respective character
                    text = text.replace("\u002c", ",")
                    text = text.replace("\u2019", "'")
                    text = text.replace('\\"\"', '"')
                    is_test_set = True
                    text = text.strip()
                    if "\u" in text:
                        print text
            else:
                raise ValueError("Dataset has wrong format for task 4a")
            # Consider only tweets for which the messages could be retrieved
            if text != "Not Available":
                if ncol == 3 and not is_test_set:
                    counts[label] += 1
                tweets.append(t)
    print "Label distribution:", counts
    print "total tweets", len(tweets)
    return tweets


if __name__ == "__main__":
    task_a = "../semeval2016_task4/train/tweets.txt"
    # For trial data
    # tweets_a, idx_a = open_cleansed_tsv(task_a)
    # print "#Tweets in task 4, subtask A;", len(tweets_a)
    # for tweet in tweets_a:
    #     print tweet.user_id, tweet.tweet_id, tweet.label
    # print len(idx_a), idx_a

    # For real data
    tweets = open_dataset(task_a)
    # for tweet in tweets:
    #     print tweet.tweet_id, "-", tweet.label, "-", tweet.text
    print "tweets: ", len(tweets)