"""
Turns the dataset into a bag-of-words representation
"""
import copy
from Data_task_4.feature_selection.cpd import CPD

from Data_task_4.preprocess.preprocessing import preprocess, dummy_preprocess, \
    preprocess_cpd
from Data_task_4.io.input_output import open_tweets_tsv
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
from Data_task_4.models.containers import Tweet
from nltk.stem.snowball import SnowballStemmer
import warnings


def _determine_preprocessor(text, stemmer=None, lang=None):
    """
    Stems the tweets as a pre-processing step.

    Parameters
    ----------
    text: A single tweet text.
    stemmer: Name of the stemmer, either None, "porter" or "snowball".
    lang: Language to be used in stemmer, e.g. "english".

    Returns
    -------
    Returns a single stemmed (or unstemmed) tweet text.
    """
    # if stemmer is not None:
    #     if stemmer == "porter":
    #         stemmer_ = SnowballStemmer("porter")
    #     elif lang is not None:
    #         stemmer_ = SnowballStemmer(lang)
    #     else:
    #         warnings.warn(
    #             "No stemmer language was specified!. Let's use English.")
    #         stemmer_ = SnowballStemmer("english")
    #     # for tweet in tweets:
    #     stemmed = " ".join([stemmer_.stem(word) for word in text.split()])
    #     print "stemmed text", stemmed
    #     return stemmed
    return text


def to_bag_of_words(tweets, options, selected_tweets=None,
                    vectorizer=None,
                    do_train=True, use_cpd=False, cpd_threshold=0.0):
    """
    Converts the tweets into the respective bag-of-words representation and
    stores it as an extra field. Additionally, a list of all words used in
    the corpus is stored as well.

    Parameters
    ----------
    tweets: List of tweet objects that should be converted.
    options: Option object containing all options regarding BoW.
    selected_tweets: List with tweets indices to be considered. If None,
                     then all tweets are used, otherwise only the indexed
                     tweets will be used from tweets.
    vectorizer: CountVectorizer object. If not None, it is assumed it was
                instantiated earlier, so it'll be reused to convert the new
                tweets into BoW representation using the old vocabulary.
    do_train: True, if vectorizer should first learn the vocabulary and then
              convert the tweets into BoW representation. False, if it should
              only convert the tweets into BoW representation using existing
              vocabulary. In other words, the vectorizer already contains
              some vocabulary. This is necessary when converting a test set
              into the same BoW representation as its training set (on which
              the vectorizer was trained). Default = False
    no_preprocessing: True, if the tweets should be directly converted into
                      BoW without any further preprocessing.
    use_alternative_rep: True if no BoW information should be used but only
                         additional features.
    use_cpd: If True, Conditional Proportional Difference, is used to reduce
             the feature set.
    cpd_threshold: Threshold for CPD when to discard a word. 0.0 = Keep all,
                   1.0 = remove all that don't occur only within a single class.

    Returns
    -------
    List, CountVectorizer.
    List of tweet objects enriched with the transformed BoW representation
    which is accessible via the attribute "bow".
    CountVectorizer object fitted given the tweet corpus, allowing to apply
    it to unknown data later on. Transforms any unknown data into the EXACT
    same representation as the tweet corpus it was built with.

    """
    # Use only the tweets indexed in selected_tweets as corpus
    if selected_tweets is not None:
        tweets = [tweets[i] for i in selected_tweets]

    # Reduce feature space with CPD
    removed_words = {}
    if use_cpd:
        preprocess_cpd(tweets, options)
        cpd = CPD(tweets, cpd_threshold)
        print "#words with threshold {}".format(cpd_threshold)
        removed_words = cpd.create()

    for idx, tweet in enumerate(tweets):
        # Tokenize each tweet
        if options.no_preprocessing:
            preprocessed = dummy_preprocess(tweet)
        else:
            preprocessed = preprocess(tweet, options)
        if options.alternative_bow:
            # Don't store any BoW information by passing in empty documents
            tweet.preprocessed = ""
        else:
            tweet.preprocessed = preprocessed[0]
        tweet.pos_emoticons = preprocessed[1]
        tweet.neg_emoticons = preprocessed[2]
        tweet.punctuations = preprocessed[3]
        tweet.start_len = preprocessed[4]
        tweet.end_len = preprocessed[5]
        tweet.avg_len = preprocessed[6]
        tweet.adj_percentage = preprocessed[7]
        tweet.adv_percentage = preprocessed[8]
        tweet.verb_percentage = preprocessed[9]
        tweet.noun_percentage = preprocessed[10]
        tweet.has_hashtag = preprocessed[11]
        tweet.alchemy_tweet_polarity = preprocessed[12]
        tweet.alchemy_tweet_type = preprocessed[13]
        tweet.alchemy_entity_polarity = preprocessed[14]
        tweet.alchemy_entity_type = preprocessed[15]
        tweet.mixed_sentiment = preprocessed[16]
        # Tweet polarity features (13)
        tweet.neg = preprocessed[17][0]
        tweet.obj = preprocessed[17][1]
        tweet.pos = preprocessed[17][2]
        tweet.elongated = preprocessed[17][3]
        tweet.upper = preprocessed[17][4]
        tweet.negative_words = preprocessed[17][5]
        tweet.objective_words = preprocessed[17][6]
        tweet.positive_words = preprocessed[17][7]
        tweet.polarity_words = preprocessed[17][8]
        tweet.negation_words = preprocessed[17][9]
        tweet.negative_words_sum = preprocessed[17][10]
        tweet.objective_words_sum = preprocessed[17][11]
        tweet.positive_words_sum = preprocessed[17][12]
        # Hashtag polarity features (11)
        tweet.neg_hash = preprocessed[18][0]
        tweet.obj_hash = preprocessed[18][1]
        tweet.pos_hash = preprocessed[18][2]
        tweet.negative_words_hash = preprocessed[18][3]
        tweet.objective_words_hash = preprocessed[18][4]
        tweet.positive_words_hash = preprocessed[18][5]
        tweet.polarity_words_hash = preprocessed[18][6]
        tweet.negation_words_hash = preprocessed[18][7]
        tweet.negative_words_sum_hash = preprocessed[18][8]
        tweet.objective_words_sum_hash = preprocessed[18][9]
        tweet.positive_words_sum_hash = preprocessed[18][10]
        print "preprocessed tweets:", (idx+1), preprocessed
    if do_train:
        print "train new vectorizer"
        # Learn the new vocabulary
        train_data_features = vectorizer.fit(
            tweets)
    else:
        print "apply existing vectorizer"
        # Vectorizer exists already -> DON'T learn the new vocabulary
        train_data_features = vectorizer.transform(
            tweets)
    # Numpy arrays are easy to work with, so convert the result to an
    # array
    train_data_features = train_data_features.toarray()
    # Add the bag-of-words representation to each tweet as a list
    # The order is identical because it's based on the order of tweet_texts
    for idx, tweet in enumerate(tweets):
        tweet.bow = train_data_features[idx]
    # Take a look at the words in the vocabulary
    # vocab = vectorizer.get_feature_names()
    # print "words in the vocabulary:", len(vocab)
    # print vocab
    # Add the list of used words in the corpus to our tweet
    # Tweet.vocabulary = vectorizer.vocabulary_
    # print "list of words in all tweets:", tweets[0].words
    # Sum up the counts of each vocabulary word
    # dist = np.sum(train_data_features, axis=0)
    # For each, print the vocabulary word and the number of times it
    # appears in the training set
    # print "word frequencies in whole corpus:"
    # for tag, count in zip(vocab, dist):
    #     print "{}: {}".format(tag, count)
    # print "bag-of-words representation per tweet:"
    # print train_data_features
    # print train_data_features.shape
    print "transformed tweets:", train_data_features.shape[0]
    # print "#words in bag-of-words vector:", train_data_features.shape[1]
    # print "Bag-of-words representation per tweet:"
    # for t in tweets:
    #     print t.bow
    return tweets, vectorizer


def to_bag_of_words_both(tweets, options, selected_tweets=None,
                    vectorizer=None, vectorizer_alt=None,
                    do_train=True, use_cpd=False, cpd_threshold=0.0,
                         removed_words=None):
    """
    Converts the tweets into the respective bag-of-words representation and
    stores it as an extra field. Additionally, a list of all words used in
    the corpus is stored as well. As opposed to "to_bag_of_words()",
    it builds the desired representation as well as an alternative
    representation.

    Parameters
    ----------
    tweets: List of tweet objects that should be converted.
    options: Option object containing all options regarding BoW.
    selected_tweets: List with tweets indices to be considered. If None,
                     then all tweets are used, otherwise only the indexed
                     tweets will be used from tweets.
    vectorizer: BoW object. If not None, it is assumed it was
                instantiated earlier, so it'll be reused to convert the new
                tweets into BoW representation using the old vocabulary.
    vectorizer_alt: Vectorizer object for alternative representation. See
                    <vectorizer for more information.
    do_train: True, if vectorizer should first learn the vocabulary and then
              convert the tweets into BoW representation. False, if it should
              only convert the tweets into BoW representation using existing
              vocabulary. In other words, the vectorizer already contains
              some vocabulary. This is necessary when converting a test set
              into the same BoW representation as its training set (on which
              the vectorizer was trained). Default = False
    no_preprocessing: True, if the tweets should be directly converted into
                      BoW without any further preprocessing.
    use_alternative_rep: True if no BoW information should be used but only
                         additional features.
    use_cpd: If True, Conditional Proportional Difference, is used to reduce
             the feature set.
    cpd_threshold: Threshold for CPD when to discard a word. 0.0 = Keep all,
                   1.0 = remove all that don't occur only within a single class.
    removed_words: Dictionary containing words to be removed. If None,
    it'll be created if CPD is enabled.

    Returns
    -------
    List, List, BoW, BoW.
    List of tweet objects enriched with the transformed BoW representation
    which is accessible via the attribute "bow" - for the desired
    representation.
    List of tweet objects for the alternative representation.
    BoW object fitted given the tweet corpus, allowing to apply
    it to unknown data later on. Transforms any unknown data into the EXACT
    same representation as the tweet corpus it was built with - for the
    desired representation.
    BoW object for the alternative representation.

    """
    # Use only the tweets indexed in selected_tweets as corpus
    if selected_tweets is not None:
        tweets = [tweets[i] for i in selected_tweets]

    if removed_words is None:
        removed_words = {}
    # Reduce feature space with CPD
    # No words determined yet and CPD should be used
    if len(removed_words) == 0 and use_cpd:
        preprocess_cpd(tweets, options)
        cpd = CPD(tweets, cpd_threshold)
        print "#words with threshold {}".format(cpd_threshold)
        removed_words = cpd.create()
    # Create a list of all tweet messages
    for idx, tweet in enumerate(tweets):
        # Tokenize each tweet
        if options.no_preprocessing:
            preprocessed = dummy_preprocess(tweet)
        else:
            preprocessed = preprocess(tweet, options, removed_words)
        print "preprocessed tweets:", (idx+1), preprocessed
        tweet.preprocessed = preprocessed[0]
        tweet.pos_emoticons = preprocessed[1]
        tweet.neg_emoticons = preprocessed[2]
        tweet.punctuations = preprocessed[3]
        tweet.start_len = preprocessed[4]
        tweet.end_len = preprocessed[5]
        tweet.avg_len = preprocessed[6]
        tweet.adj_percentage = preprocessed[7]
        tweet.adv_percentage = preprocessed[8]
        tweet.verb_percentage = preprocessed[9]
        tweet.noun_percentage = preprocessed[10]
        tweet.has_hashtag = preprocessed[11]
        tweet.alchemy_tweet_polarity = preprocessed[12]
        tweet.alchemy_tweet_type = preprocessed[13]
        tweet.alchemy_entity_polarity = preprocessed[14]
        tweet.alchemy_entity_type = preprocessed[15]
        tweet.negative_entities = preprocessed[16]
        tweet.neutral_entities = preprocessed[17]
        tweet.positive_entities = preprocessed[18]
        tweet.mixed_sentiment = preprocessed[19]
        # Tweet polarity features (13)
        tweet.neg = preprocessed[20][0]
        tweet.obj = preprocessed[20][1]
        tweet.pos = preprocessed[20][2]
        tweet.elongated = preprocessed[20][3]
        tweet.upper = preprocessed[20][4]
        tweet.negative_words = preprocessed[20][5]
        tweet.objective_words = preprocessed[20][6]
        tweet.positive_words = preprocessed[20][7]
        tweet.polarity_words = preprocessed[20][8]
        tweet.negation_words = preprocessed[20][9]
        tweet.negative_words_sum = preprocessed[20][10]
        tweet.objective_words_sum = preprocessed[20][11]
        tweet.positive_words_sum = preprocessed[20][12]
        # Hashtag polarity features (11)
        tweet.neg_hash = preprocessed[21][0]
        tweet.obj_hash = preprocessed[21][1]
        tweet.pos_hash = preprocessed[21][2]
        tweet.negative_words_hash = preprocessed[21][3]
        tweet.objective_words_hash = preprocessed[21][4]
        tweet.positive_words_hash = preprocessed[21][5]
        tweet.polarity_words_hash = preprocessed[21][6]
        tweet.negation_words_hash = preprocessed[21][7]
        tweet.negative_words_sum_hash = preprocessed[21][8]
        tweet.objective_words_sum_hash = preprocessed[21][9]
        tweet.positive_words_sum_hash = preprocessed[21][10]
    # Also create the alternative representation now because it's for
    # free and saves almost a day of preprocessing! It has exactly the same
    # features, only the text to be converted into BoW is empty
    tweets_alt = copy.deepcopy(tweets)
    for t in tweets_alt:
        t.preprocessed = ""
    if do_train:
        print "train new vectorizer"
        train_data_features = vectorizer.fit(
            tweets)
        train_alternative = vectorizer_alt.fit(tweets_alt)
        # Store words to be removed. TODO: extend BoW vectorizer to ignore them
        vectorizer.removed_words = removed_words
        vectorizer_alt.removed_words = removed_words
    else:
        print "apply existing vectorizer"
        # Vectorizer exists already -> DON'T learn the new vocabulary
        train_data_features = vectorizer.transform(
            tweets)
        train_alternative = vectorizer_alt.transform(tweets_alt)
    # Numpy arrays are easy to work with, so convert the result to an
    # array
    train_data_features = train_data_features.toarray()
    train_alternative = train_alternative.toarray()
    # Add the bag-of-words representation to each tweet as a list
    # The order is identical because it's based on the order of tweets
    for idx, tweet in enumerate(tweets):
        tweet.bow = train_data_features[idx]
    for idx, tweet in enumerate(tweets_alt):
        tweet.bow = train_alternative[idx]
    # Take a look at the words in the vocabulary
    # vocab = vectorizer.get_feature_names()
    # print "words in the vocabulary:", len(vocab)
    # print vocab
    # Add the list of used words in the corpus to our tweet
    # Tweet.vocabulary = vectorizer.vocabulary_
    # print "list of words in all tweets:", tweets[0].words
    # Sum up the counts of each vocabulary word
    # dist = np.sum(train_data_features, axis=0)
    # For each, print the vocabulary word and the number of times it
    # appears in the training set
    # print "word frequencies in whole corpus:"
    # for tag, count in zip(vocab, dist):
    #     print "{}: {}".format(tag, count)
    # print "bag-of-words representation per tweet:"
    # print train_data_features
    # print train_data_features.shape
    print "transformed tweets:", train_data_features.shape[0]

    return tweets, tweets_alt, vectorizer, vectorizer_alt


if __name__ == "__main__":
    pass
