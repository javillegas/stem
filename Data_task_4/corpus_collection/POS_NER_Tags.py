import sys
import cPickle
reload(sys)
sys.setdefaultencoding("utf-8")

import nltk
from nltk.tag.stanford import StanfordPOSTagger, StanfordNERTagger


def lemmatize(sentence):
    stc_tokens = nltk.word_tokenize(sentence)
    # print "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    # print "Tokens: ", stc_tokens
    return stc_tokens


def POSTags(path, sentence):
    POS = StanfordPOSTagger(path +
                            'models/english-bidirectional-distsim.tagger',
                            path + 'stanford-postagger.jar')
    stc_POS = POS.tag(sentence)
    # print "POS tagged sentence: ", stc_POS
    return stc_POS


def NERTags(path, sentence):
    NER = StanfordNERTagger(
        path + 'classifiers/english.conll.4class.distsim.crf.ser.gz', path +
        'stanford-ner.jar')
    stc_NER = NER.tag(sentence)
    # print "NERs in sentence: ", stc_NER
    return stc_NER


def preprocess_ner(sentence, out_file):
    """
    Pre-processes a list of tweet messages and stores the NER representation
    in a file - 1 line represents 1 tweet.

    """
    Stc_Tok = []
    Stc_POSTags = []
    Stc_NERTags = []

    # path1 = '/home/mishalkazmi/NLP/PyCharm/inspire/semeval/task2/resources/stanford-postagger-full-2015-04-20/'
    # path2 = '/home/mishalkazmi/NLP/PyCharm/inspire/semeval/task2/resources/stanford-ner-2015-04-20/'

    path1 = '/home/fensta/Stanford/stanford-postagger-full-2015-04-20/'
    path2 = '/home/fensta/Stanford/stanford-ner-2015-04-20/'
    for s in sentence:
        stc_tokens = lemmatize(s)
        Stc_Tok.append(stc_tokens)
        stc_POS = POSTags(path1, stc_tokens)
        Stc_POSTags.append(stc_POS)
        stc_NER = NERTags(path2, stc_tokens)
        Stc_NERTags.append(stc_NER)
        with open(out_file, "wb") as f:
            cPickle.dump(Stc_NERTags, f)
    # return Stc_Tok, Stc_POSTags, Stc_NERTags


if __name__ == "__main__":
    content = open('test_preprocessed.txt', 'r')
    tweets = content.readlines()
    preprocess_ner(tweets, "ner_test_tags.pkl")
    # stc_tok, stc_POS, stc_NER = preprocess(stc1)
