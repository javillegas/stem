# -*- coding: utf-8 -*-
import tweepy
import os
import json
from oauth import TwitterAuth
import codecs

class TweetListener(tweepy.StreamListener):
    """
    Collects tweets and stores them in separate files per tweet under a 
    specified directory.
    """
    def __init__(self, out_file, api=None):
        """
        
        Listens for new tweets regarding specific hashtags. It stores each 
        tweet message in a predefined file appending them
        
        Parameters:
        -----------
        out_file: Path to file in which tweet messages are stored.
        api: Credentials used for establising a connection to Twitter.
        
        """
        self.auth = TwitterAuth()
        self.api = api or tweepy.API()
        self.collected_tweets = 0
        self.out_file = out_file
        super(tweepy.StreamListener, self).__init__()
    
    
    def on_data(self, data):
        if 'in_reply_to_status' in data:
            self.on_status(data) # This line is essential to retrieve the raw 
                                 # JSON. Otherwise, a Status object is passed 
                                 # to on_status
#        elif 'delete' in data:
#            delete = json.loads(data)['delete']['status']
#            if self.on_delete(delete['id'], delete['user_id']) is False:
#                return False
#        elif 'limit' in data:
#            if self.on_limit(json.loads(data)['limit']['track']) is False:
#                return False
#        elif 'warning' in data:
#            warning = json.loads(data)['warnings']
#            print warning['message']
#            return false    
    
    
    def on_status(self, data):
        """
        Invoked when a new tweet is received. 
        """
        # Twitter returns data in JSON format
        data = json.loads(data)
        if data["lang"] == "en": # Store only English tweets
            # User ID _ Tweet ID.txt
            with codecs.open(self.out_file, "a", encoding="utf-8") as f:
                # Remove newlines and add newline at the end, so that ideally
                #  1 tweet is stored per line
                text = data["text"].rstrip("\r\n") + "\n"
                f.write(text)
            self.collected_tweets += 1
            print "Collected tweets: ", self.collected_tweets
        return
    
    
    def on_error(self, status):
        """
        Invoked when an error occurs in the stream.
        """
        # Switch credentials
        print "error:",status
        self.api = self.auth.circle_login()
        return  # Don't stop the stream
    
    
    def on_timeout(self):
        """
        Invoked when rate limit is exceeded.
        """
        # Switch credentials
        self.api = self.auth.circle_login()
        return  # Don't stop the stream