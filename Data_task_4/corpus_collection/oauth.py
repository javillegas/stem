# -*- coding: utf-8 -*-
"""
Accomplishes Twitter authentication using different keys
"""
import tweepy

class TwitterAuth:
    
    def __init__(self):
        self.oauth_keys = \
        {
        0:
        {"consumer_key":"XXX",
         "consumer_secret":"XXX",
         "access_token":"XXX",
         "access_token_secret":"XXX",
        },
        
        }
        self.current_credentials = -1 # We start manually with #22 and continue 
                                      # with #0
        self.keys = len(self.oauth_keys) - 1
        
    
    def circle_login(self):
        """
        
        Circles through all available credentials to bypass rate limit.
        
        Returns:
        --------
        New credentials for establishing a connection
        
        """
        # Try to connect with next oauth keys
        new_credentials = (self.current_credentials + 1) % self.keys
        self.current_credentials = new_credentials
        print "now using oauth #",new_credentials
        auth = tweepy.auth.OAuthHandler(
            self.oauth_keys[new_credentials]["consumer_key"], 
            self.oauth_keys[new_credentials]["consumer_secret"])
        auth.set_access_token(
            self.oauth_keys[new_credentials]["access_token"], 
            self.oauth_keys[new_credentials]["access_token_secret"])
        api = tweepy.API(auth)
        return api
