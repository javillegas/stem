"""
Implements certainty sampling when labels are already available (=
simulation of AL). Uses confidence/margin to measure certainty.

The idea is to apply AL to identify reliable instances which can be used for
training a supervised model afterwards.
"""
import cPickle
import copy

from sklearn.naive_bayes import MultinomialNB
from sklearn.multiclass import OneVsOneClassifier, OneVsRestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.metrics import f1_score, confusion_matrix
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm

from Data_task_4.experiments.run_experiments_own_bow import _get_cv_iterator,\
    _calculate_f1


class CertaintySampling(object):
    def __init__(self, clf, seed_set, budget, al_type="margin", seed=42):
        """
        Initializes the certainty sampling strategy.

        Parameters
        ----------
        clf: Classifier to be used for the AL strategy.
        seed_set: List of Tweet objects used for learning initial model.
        unlabeled: List of Tweet objects forming the pool of unlabeled
                   instances
        budget: Number of instances for which a label should be acquired.
        test: List of Tweet objects used as test set to evaluate the strategy.
        type: Metric to be used for calculating certainty: "confidence",
              "margin".
        seed: Initial value for PRNG.

        """
        self.seed = seed
        self.al_type = al_type
        # self.unlabeled_tweets = unlabeled
        # self.unlabeled_tweets = []
        self.clf = clf
        self.budget = budget
        # Use this one for cross-validation
        self.budget_cv = budget
        # Labeled pool
        self.X_l = np.asarray([t.bow for t in seed_set])
        self.y_l = np.asarray([t.label for t in seed_set])
        # In cross-validation, work with these values in order not to leak
        # any information across folds
        self.X_l_cv = self.X_l
        self.y_l_cv = self.y_l
        # List containing all Tweet objects with labels after AL
        self.labeled_tweets = seed_set
        # Without deep copy, a shallow copy is created!
        self.labeled_tweets_cv = copy.deepcopy(self.labeled_tweets)
        # Contains tweet IDs of tweets that are in the labeled pool
        self.seen = set([t.tweet_id for t in seed_set])
        self.seen_cv = set([t.tweet_id for t in seed_set])
        # Note, AFTER TRAINING THE FULL MODEL, ALL INSTANCES CONTAINED IN
        # self.X_l, self.y_l and self.labeled_tweets REPRESENT THE TRAINING
        # SET WITH LESS NOISE

        # Train initial classifier on seed set
        self.clf.fit(self.X_l, self.y_l)

    def _move_from_unlabeled_to_labeled(self, X_u, y_u, unlabeled_tweets, idx):
        """
        Moves tweet from unlabeled to labeled pool. Use this when not using CV!
        """
        # Remove from unlabled pool
        tweet = unlabeled_tweets.pop(idx)
        bow = X_u[idx]
        label = y_u[idx]
        X_u = np.delete(X_u, idx, axis=0)
        y_u = np.delete(y_u, idx, axis=0)

        # Add to labeled pool
        self.labeled_tweets.append(tweet)
        self.seen.add(tweet.tweet_id)
        self.X_l = np.vstack((self.X_l, bow))
        self.y_l = np.hstack((self.y_l, label))
        return X_u, y_u

    def _move_from_unlabeled_to_labeled_cv(self, X_u, y_u, unlabeled_tweets,
                                          idx):
        """
        Moves tweet from unlabeled to labeled pool. Use this method for CV!

        Parameters
        ----------
        X_u: Array representing full unlabeled pool of tweets.
        y_u: Array describing the respective labels for <X_u>.
        unlabeled_tweets: List of Tweet objects. Contains as many elements as
                          <X_u>.
        idx: Index of the tweet that should be moved from unlabeled to
             labeled pool.

        Returns
        -------
        np.array, np.array.
        Array representing updated unlabeled pool [n_instances, n_features]
        and correspondingly updated labels after removing the instance.
        """
        # Remove from unlabled pool
        tweet = unlabeled_tweets.pop(idx)
        bow = X_u[idx]
        label = y_u[idx]
        X_u = np.delete(X_u, idx, axis=0)
        y_u = np.delete(y_u, idx, axis=0)

        # Add to labeled pool for CV
        self.labeled_tweets_cv.append(tweet)
        self.seen_cv.add(tweet.tweet_id)
        self.X_l_cv = np.vstack((self.X_l_cv, bow))
        self.y_l_cv = np.hstack((self.y_l_cv, label))
        return X_u, y_u

    def evaluate(self, X_t, y_t, clf):
        """
        Parameters
        ----------
        X_t: array-like, [n_tweets, n_features] representing Tweet objects
             for testing the performance of the model.
        y_t: Corresponding true labels for <X_t> in the same order like <X_t>.
        clf: Scikit classifier object.

        Returns
        -------
        int, numpy.array.
        Weighted F1 score for the fold and the respective confusion matrix.

        """
        preds = clf.predict(X_t)
        res = f1_score(y_t, preds, labels=None, pos_label=None,
                           average="weighted")
        CLASSES = ["negative", "neutral", "positive"]
        cm = confusion_matrix(y_t, preds, labels=np.array(CLASSES))
        return res, cm

    def _get_most_informative_tweet(self, X_u, tweets):
        """
        Determines the most informative tweet among the unlabeled instances.

        X_u: array-like shape [n_instances, n_features] representing
             unlabeled dataset.
        tweets: List of Tweet objects representing unlabeled pool.

        Returns
        -------
        int.
        Index of unlabeled tweet with highest gain.
        """
        # Use confidence to calculate certainty
        if self.al_type == "margin":
            # print "get instance by margin"
            idx = self.get_margin_idx(X_u, tweets)
        if self.al_type == "confidence":
            # print "get instance by confidence"
            idx = self.get_conf_idx(X_u, tweets)
        return idx

    def get_margin_idx(self, X_u, tweets):
        """
        Use margin of a classifier to select the most certain instance.

        Parameters
        ----------
        X_u: array-like shape [n_instances, n_features] representing
             unlabeled dataset.

        Returns
        -------
        int.
        Index of tweet with lowest margin.

        Raises
        ------
        NotImplementedError: If one tries to calculate confidence for a
        classifier other than SVM, logistic regression and MNB if they are
        used in conjunction with OVR or OVO.

        """
        # List of classifier confidences when predicting labels for all
        # unlabeled instances
        margs = []
        # For SVM, logistic regression use decision function to obtain
        # confidence
        if isinstance(self.clf, OneVsOneClassifier) or \
                isinstance(self.clf, OneVsRestClassifier):
            # For SVM and logistic regression
            if isinstance(self.clf.get_params()["estimator"], SVC) or \
                    isinstance(self.clf, LogisticRegression):
                margs = self.clf.decision_function(X_u)
            # # For multinomial naive bayes use predicted probability
            if isinstance(self.clf.get_params()["estimator"],
                          MultinomialNB):
                margs = self.clf.predict_proba(X_u)
        # Logistic regression
        elif isinstance(self.clf, LogisticRegression):
            margs = self.clf.predict_proba(X_u)
        else:
            raise NotImplementedError("Confidence/margin is only implemented "
                                      "for "
                                      "multi-class problems!")
        # print "margins", margs
        return self._get_next_margin_tweet(margs, tweets)

    def get_conf_idx(self, X_u, tweets):
        """
        Use confidence of a classifier to select the most certain instance.

        Parameters
        ----------
        X_u: array-like shape [n_instances, n_features] representing
             unlabeled dataset.
        tweets: List of Tweet objects representing unlabeled pool.

        Returns
        -------
        int.
        Index of tweet with lowest margin.

        Raises
        ------
        NotImplementedError: If one tries to calculate confidence for a
        classifier other than SVM, logistic regression and MNB if they are
        used in conjunction with OVR or OVO.

        """
        # List of classifier confidences when predicting labels for all
        # unlabeled instances
        confs = []
        # For SVM, logistic regression use decision function to obtain
        # confidence
        if isinstance(self.clf, OneVsOneClassifier) or \
                isinstance(self.clf, OneVsRestClassifier):
            # For SVM and logistic regression
            if isinstance(self.clf.get_params()["estimator"], SVC) or \
                    isinstance(self.clf, LogisticRegression):
                confs = self.clf.predict_proba(X_u)
            # # For multinomial naive bayes use predicted probability
            if isinstance(self.clf.get_params()["estimator"],
                          MultinomialNB):
                confs = self.clf.predict_proba(X_u)
        # Logistic regression
        elif isinstance(self.clf, LogisticRegression):
            confs = self.clf.predict_proba(X_u)
        else:
            raise NotImplementedError("Confidence/margin is only implemented "
                                      "for "
                                      "multi-class problems!")
        # print "confidences", confs
        return self._get_next_conf_tweet(confs, tweets)

    def _get_next_margin_tweet(self, margs, tweets):
        """
        Chooses instance with highest margin between 2 most probable classes.

        Parameters
        ----------
        margs: List of margins with which the classifier predicted the
               labels of the unlabeled instances. Contains a probability for
               each class where the sum of all probabilities equals 1.
        tweets: List of Tweet objects representing unlabeled pool.

        Returns
        -------
        int.
        Index of instance with highest margin.

        """
        max_marg = 0
        max_idx = 0
        for idx, marg_vals in enumerate(margs):
            sorted_marg_vals = np.sort(marg_vals)[::-1]
            # print "current confidence {} at {}".format(conf_vals, idx)
            # print "min confidence", min_conf
            # If there are k classes, k confidence values exist -> choose
            # minimal value
            # conf = min(conf_vals)
            marg = sorted_marg_vals[0] - sorted_marg_vals[1]
            if marg > max_marg:
                # Can only happen on full training set that a labeled tweet
                # is picked again
                if not tweets[idx].tweet_id in self.seen:
                    max_marg = marg
                    max_idx = idx
        return max_idx

    def _get_next_conf_tweet(self, confs, tweets):
        """
        Choose the instance with the highest confidence. Hence, select the
        instance where the classifier's highest confidence is the highest.

        Parameters
        ----------
        confs: List of confidences with which the classifier predicted the
               labels of the unlabeled instances.
        tweets: List of Tweet objects representing unlabeled pool.

        Returns
        -------
        int.
        Index of instance with highest confidence.

        """
        max_conf = 0
        max_idx = 0
        for idx, conf_vals in enumerate(confs):
            # sorted_conf_vals = np.sort(conf_vals)[::-1]
            # print "current confidence {} at {}".format(conf_vals, idx)
            # print "min confidence", min_conf
            # If there are k classes, k confidence values exist -> choose
            # maximal value
            conf = max(conf_vals)
            # conf = sorted_conf_vals[0] - sorted_conf_vals[1]
            if conf > max_conf:
                # Can only happen on full training set that a labeled tweet
                # is picked again
                if not tweets[idx].tweet_id in self.seen:
                    max_conf = conf
                    max_idx = idx
        return max_idx

    def _create_arrays(self, X, y, tweets, tweets_fold, idx, to_train=True):
        """
        Creates 3 lists of data necessary for the computations: the BoW
        representation of the tweets, their labels, the Tweet objects.

        Parameters
        ----------
        X: List of BoW representation of tweets.
        y: List of corresponding labels.
        tweets: List of Tweet objects.
        tweets_fold: Tweet objects to be used in fold.
        idx: List of indices of the instances that should be used.
        to_train: True, if lists should be created for unlabeled pool

        Returns
        -------
        np.array, np.array

        """
        X_fold = []
        y_fold = []
        for i in idx:
            use = True
            # Add only if tweet isn't part of labeled pool yet
            if to_train and tweets[i].tweet_id in self.seen_cv:
                use = False
            if use:
                X_fold.append(X[i])
                y_fold.append(y[i])
                tweets_fold.append(tweets[i])
        return np.asarray(X_fold), np.asarray(y_fold)

    def al_train(self, X, y, tweets, clf, cv=True):
        """
        Trains a given classifier.

        Parameters
        ----------
        X: BoW representation of unlabeled tweets as a list of numpy.arrays.
        y: List of corresponding labels for <X>.
        tweets: List of tweet objects.
        clf: Scikit classifier object.
        cv: If True, it's assumed that a classifier will be trained for a
            single fold. If False, the final classifier will be trained.

        """
        # AL loop
        if cv:
            budget = self.budget_cv
        else:
            budget = self.budget
        print "initial budget", budget
        print "training instances to choose from", len(X)
        while budget > 0 and len(X) > 0:
            # print "Remaining budget:", budget
            # Get most informative instance -> will be labeled next
            idx = self._get_most_informative_tweet(X, tweets)
            # print "Tweets in unlabeled pool before removal", len(X_u_fold)
            # print "Tweets in labeled pool before adding", len(self.X_l)
            if cv:
                X, y = self._move_from_unlabeled_to_labeled_cv(
                    X, y, tweets, idx)
                # print "Tweets in unlabeled pool after removal", len(X_u_fold)
                # print "Tweets in labeled pool after adding", len(self.X_l)
                clf.fit(self.X_l_cv, self.y_l_cv)
            else:
                X, y = self._move_from_unlabeled_to_labeled(
                    X, y, tweets, idx)
                # print "Tweets in unlabeled pool after removal", len(X_u_fold)
                # print "Tweets in labeled pool after adding", len(self.X_l)
                clf.fit(self.X_l, self.y_l)
            budget -= 1

    def cv_certainty(self, unlabeled, k=10, cv_type="k", test=None):
        """
        Performs k-fold cross-validation.

        Parameters
        ----------
        unlabeled: List of Tweet objects that could be labeled. (=unlabeled
                   pool).
        k: Number of iterations in cross-validation.
        cv_type: Type of cross-validation: "shuffle" for random
                 permutations CV, "k" for simple k-fold CV,
                 "bootstrap" for bootstrapped CV or
                 "stratified" for stratified CV. Default = "k".
        test: List of Tweet objects that should be used for as a hold-out set
              for testing the learned classifier at the end. None means no
              test set will be used.

        Returns
        -------
        double, np.array.
        Weighted average F1 score of the strategy and the
        corresponding confusion matrix.

        """
        X_u = np.asarray([t.bow for t in unlabeled])
        y_u = np.asarray([t.label for t in unlabeled])
        # unlabeled_tweets = unlabeled
        CLASSES = ["negative", "neutral", "positive"]
        cv = _get_cv_iterator(len(X_u), cv_type=cv_type, folds=k)
        total_cm =np.zeros((len(CLASSES), len(CLASSES)))
        total_f1 = []
        for fold, (train_idx, test_idx) in enumerate(cv):
            print train_idx, test_idx
            # Create a new copy of classifier trained on seed set in order not
            # to learn from other folds
            clf = copy.deepcopy(self.clf)
            # Reset labeled pool in each fold, otherwise samples will remain
            # there
            self.labeled_tweets_cv = []
            self.X_l_cv = copy.deepcopy(self.X_l)
            self.y_l_cv = copy.deepcopy(self.y_l)
            unlabeled_tweets_fold = []
            test_tweets_fold = []
            # In each fold only the seed set was initially seen
            self.seen_cv = copy.deepcopy(self.seen)
            # Create unlabeled pool for fold
            X_u_fold, y_u_fold = self._create_arrays(
                X_u, y_u, unlabeled, unlabeled_tweets_fold, train_idx)
            # Create test set for fold
            X_t_fold, y_t_fold = self._create_arrays(
                X_u, y_u, unlabeled, test_tweets_fold, test_idx)
            # Training
            self.al_train(X_u_fold, y_u_fold, unlabeled_tweets_fold, clf)
            f1, cm = self.evaluate(X_t_fold, y_t_fold, clf)
            print "F1 in fold {}: {}".format(fold + 1, f1)
            print "Confusion matrix in fold {}:". format(fold+1)
            print cm
            total_cm += cm
            total_f1.append(f1)
        f1_cls_scores, avg_f1 = _calculate_f1(total_cm, np.array(
        CLASSES))
        print "Biased F1"
        print total_f1
        print np.mean(total_f1)
        for cls_idx, cls in enumerate(CLASSES):
            print "Class {}: {}".format(cls, f1_cls_scores[cls_idx])
        print "Weighted average F1 on training set: ", avg_f1
        # Learn classifier now on the whole labeled data
        self.al_train(X_u, y_u, unlabeled, self.clf, cv=False)

        # Now test classifier on separate test set
        if test is not None:
            print "Now test on test set"
            X_t = np.asarray([t.bow for t in test])
            y_t = np.asarray([t.label for t in test])
            f1, cm = self.evaluate(X_t, y_t, self.clf)
            print "Biased F1 on test set"
            print np.mean(f1)
            f1_cls_scores, avg_f1 = _calculate_f1(cm, np.array(CLASSES))
            for cls_idx, cls in enumerate(CLASSES):
                print "Class {}: {}".format(cls, f1_cls_scores[cls_idx])
            print "Weighted average F1 on test set: ", avg_f1
        else:
            print "No evaluation on separate test set, as it wasn't supplied"
        return avg_f1, cm


def plot_f1(scores, budgets, al_names, dst=None, title=None):
    """
    Plots the average F1 scores of the different AL strategies in a learning
    curve.

    Parameters
    ----------
    scores: List of lists where each list holds the average F1 scores that
            should be plotted.
    budgets: List of budgets that were used to measure the F1 scores.
    al_name: List of names to be displayed for the AL strategies. It must
             have the same order as <scores>.
    dst: Path where the file should be stored. None means it won't be stored.
    title: Additional information that should be added in the title of the plot.

    """
    title = "" if title is None else title
    plt.clf()
    # How many colors are needed?
    n = len(al_names)
    # colors = cm.rainbow(np.linspace(0, 1, n))
    colors = ["red", "blue"]
    # F1 score per strategy. Each F1 score was measured with different budgets
    for idx, avg_f1 in enumerate(scores):
        y = avg_f1
        # How many measure points exist?
        print budgets
        print y
        plt.plot(budgets, y, label="{}".format(al_names[idx]), c=colors[idx])
    plt.xlabel('Number of queries')
    plt.ylabel('Weighted average F1 score')
    plt.title('Average learning curves for our AL strategies ' + title)
    plt.legend(loc="upper center", ncol=2, bbox_to_anchor=(0.5, 1.0))
    plt.ylim(-0.05, 1.05)
    # Prevent clipping of x-axis
    plt.margins(0.1)
    plt.show()
    # Store figure
    if dst is not None:
        plt.savefig(dst)


if __name__ == "__main__":
    data_set = "../preprocessed_datasets/alt_test_cpd_0_4_3_train_full.txt"
    test_set = "../preprocessed_datasets/alt_test_cpd_0_4_3_test_full.txt"
    with open(data_set, "rb") as f:
        tweets = cPickle.load(f)
    with open(test_set, "rb") as f:
        test_tweets = cPickle.load(f)
    seed_instances = 6

    # for t in seed_set:
    #     print t.text
    budget = 5
    mnb = MultinomialNB()
    budgets = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
               19, 20, 21]
    # budgets = [1, 2]
    names = ["UC Margin (MNB)", "UC Confidence (MNB)"]
    # mnb = LogisticRegression()
    scores1 = []
    scores2 = []
    for b in budgets:
        ovr = OneVsRestClassifier(mnb)
        # Use first 6 tweets for seed set: 2 tweets from each class for 1 topic
        seed_set = tweets[:seed_instances]
        # Remainder is unlabeled pool
        unlabeled = tweets[seed_instances:]
        # ovr = LogisticRegression(multi_class="ovr")
        uc = CertaintySampling(ovr, seed_set, b, seed=42)

        f1 = uc.cv_certainty(unlabeled, k=3, test=test_tweets)
        scores1.append(f1)

        seed_set = tweets[:seed_instances]
        # Remainder is unlabeled pool
        unlabeled = tweets[seed_instances:]
        uc = CertaintySampling(ovr, seed_set, b, seed=42,
                                 al_type="confidence")
        f1 = uc.cv_certainty(unlabeled, k=3, test=test_tweets)
        scores2.append(f1)
    scores = [scores1, scores2]
    plot_f1(scores, budgets, names, title="on Test Set")
