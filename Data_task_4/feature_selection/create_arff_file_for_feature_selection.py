"""
Create an arff file to perform feature subset selection with Weka
"""
import operator
import cPickle


def to_arff(tweets, vec, dst):
    """

    Converts file under <p> into .arff format and saves it under <dst>.

    Parameters:
    -----------
    tweets: List of Tweet objects to be stored. Each element represents a
    tweet. Order of inputs is preserved, i.e. the order of the attributes
    MUST be according to the ARFF header!
    vec: List of tuples representing (vocabulary, index in matrix). It's
         assumed that this list is sorted ascendingly w.r.t. the index in the
         matrix.
    dst: Path under which arff file will be stored.

    """
    with open(dst, "w") as f:
        f.write("@RELATION features\n")
        f.write("@ATTRIBUTE id numeric\n")
        # Add remaining features according to BoW vectorizer
        for feature in vec:
            feature_name, idx = feature[0], feature[1]
            # Custom features are numbers
            # if feature_name.startswith("$$$_"):
            attr = "@ATTRIBUTE " + feature_name + " numeric"
            # else:
            # Otherwise it's a word, thus a string
            # attr = "@ATTRIBUTE " + feature_name + " numeric"
            f.write(attr + "\n")
        # Add the labels
        f.write("@ATTRIBUTE label {positive, neutral, negative}\n")
        f.write("@DATA\n\n")
        # Number of features per tweet
        feature_n = tweets[0].bow.shape[0]
        print "features per tweet:", feature_n
        for t in tweets:
            f.write(str(t.tweet_id) + ",")
            # For each feature
            for no, feature in enumerate(t.bow):
                f.write(str(feature) + ",")
                # After last item we need the tweet's label
                if no == feature_n - 1:
                    f.write(t.label + "\n")


def create_dataset(tweets, vec, dst):
    """
    Creates the arff file to perform feature subset selection in Weka.

    Parameters
    ----------
    tweets: List of Tweet objects.
    vec: BoW vectorizer object that contains the vocabulary used for the BoW
         representation of the tweets.
    dst: Output path for arff file.

    """
    vec = sorted(vec.vocabulary.items(), key=operator.itemgetter(1))
    print "VOCABULARY INDICES"
    print vec
    to_arff(tweets, vec, dst)


if __name__ == "__main__":
    prep_dir = "../preprocessed_datasets/"
    cv = "10_"
    base = "alt_submission_cpd_0_6_"
    # For creating reduced training set
    # base = "alt_sub_uc_reduced_900_"
    # base = "test"
    tweet_path = prep_dir + base + cv + "train_full.txt"
    # tweet_path = prep_dir + base + cv+ "test_full.txt"
    vec_path = prep_dir + base + cv + "bow_vectorizer_train_full.txt"
    # For creating reduced training set
    # vec_path = prep_dir + "alt_submission_cpd_0_6_" + cv + "bow_vectorizer_train_full.txt"
    with open(tweet_path, "rb") as f:
        tweets = cPickle.load(f)
    with open(vec_path, "rb") as f:
        vec = cPickle.load(f)

    dst = base + "train_full.arff"
    # dst = base + "test_full.arff"
    create_dataset(tweets, vec, dst)
