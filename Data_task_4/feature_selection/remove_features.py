"""
Removes an arbitrary number of features from a given dataset.
"""
import cPickle
import operator
import copy

import numpy as np


def remove_features(dataset, removal_indices, vec):
    """
    Removes the specified features from the dataset comprised of Tweet
    objects. Updates the respective representation.

    Parameters
    ----------
    dataset: List of Tweet objects.
    removal_indices: List of feature indices to be removed.
    vec: BoW object that stores the vocabulary and must be updated.

    """

    features_n = 0
    # Find out how many features exist
    # for tweet in dataset:
    #     features_n = len(tweet.bow)
    #     print tweet.polarity_words
    #     print tweet.neg
    #     print tweet.bow
    #     print tweet.bow.shape
    #     break
    # print "{} features were used in that dataset".format(features_n)
    # NOTE: until here the above code was only used for debugging

    # Delete respective columns per BoW representation of a tweet
    for row_idx, tweet in enumerate(dataset):
        row = np.matrix(tweet.bow, dtype=np.float64)
        # Flatten matrix into a 1D-array
        tweet.bow = np.delete(row, removal_indices, axis=1).A1
        # print tweet.bow.shape
        # print tweet.bow
        # print "....."

    # Update the vocabulary, i.e. the mapping at which index each remaining
    # feature is stored in BoW

    # Make look-up faster
    removal_indices = dict((idx, None) for idx in removal_indices)
    # print "removal indices", removal_indices
    # 1. Sort the vocabulary according to the indices ascendingly
    # vocab = sorted(vec.vocabulary.items(), key=operator.itemgetter(1))
    # print "vocabulary", vocab
    new_vocab = {}
    idx = 0
    for term, old_idx in vec.vocabulary.iteritems():
        if old_idx not in removal_indices:
            new_vocab[term] = idx
            idx += 1
    vec.vocabulary = new_vocab
    # vocab = sorted(vec.vocabulary.items(), key=operator.itemgetter(1))
    # print "updated vocabulary", vocab

if __name__ == "__main__":
    input_dataset = "../preprocessed_datasets/test_3_train_fold_1.txt"
    with open(input_dataset, "rb") as f:
        dataset = cPickle.load(f)

    # Get which attributes are stored at which positions from BoW
    with open("../preprocessed_datasets/test_3_bow_vectorizer_fold_1.txt",
              "rb") as f:
        vectorizer = cPickle.load(f)
    for term, idx in vectorizer.vocabulary.iteritems():
        print "feature '{}' has index {}".format(term, idx)
    print type(vectorizer.vocabulary["$$$_polarity_words"])
    vectorizer2 = copy.deepcopy(vectorizer)
    remove_features(dataset, [vectorizer.vocabulary["$$$_polarity_words"], 1,
                             34], vectorizer)
    vectorizer = sorted(vectorizer.vocabulary.items(),
                        key=operator.itemgetter(1))
    print "vocabulary"
    print vectorizer
    # Test working on a copy of vectorizer
    print "#################"
    for term, idx in vectorizer2.vocabulary.iteritems():
        print "feature '{}' has index {}".format(term, idx)
    remove_features(dataset, [vectorizer2.vocabulary["$$$_polarity_words"],
                              1], vectorizer2)
    vectorizer = sorted(vectorizer2.vocabulary.items(),
                        key=operator.itemgetter(1))
    print "vocabulary"
    print vectorizer