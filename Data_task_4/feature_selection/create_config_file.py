"""
Finds a good feature subset to be used for our experiments by exhaustively
testing all combinations.
"""
import shutil
import os
from collections import OrderedDict, defaultdict
import string
import itertools


def create_config(values, out_dir, clf="MNB"):
    """
    Creates a config file.

    Parameters
    ----------
    values: Dictionary containing the values that should be changed in this
    config file. CAREFUL, if properties have indent, it means they must be
    passed in as a dictionary as value where the parent property is the key.
    out_dir: Directory in which the config files should be stored.
    clf: Name of the classifier to be used. Valid values are "MNB", "LogReg"
    and "SVM".

    """
    # Default values
    min_df = 2
    max_df = 1.0
    ignore_invalid_tweets = True
    ignore_error_messages = False
    stemmer_lang = "english"
    stemmer = "snowball"
    use_bigrams = False
    more_attributes = "pos_emoticons,neg_emoticons,punctuations,start_len," \
                      "end_len,avg_len,adj_percentage,adv_percentage," \
                      "verb_percentage,noun_percentage,has_hashtag," \
                      "alchemy_tweet_polarity,alchemy_tweet_type," \
                      "alchemy_entity_polarity,alchemy_entity_type," \
                      "negative_entities,neutral_entities,positive_entities," \
                      "mixed_sentiment,neg,obj,pos,elongated,upper," \
                      "negative_words,objective_words,positive_words," \
                      "polarity_words,negation_words,negative_words_sum," \
                      "objective_words_sum,positive_words_sum,neg_hash," \
                      "obj_hash,pos_hash,negative_words_hash," \
                      "objective_words_hash,positive_words_hash," \
                      "polarity_words_hash,negation_words_hash," \
                      "negative_words_sum_hash,objective_words_sum_hash," \
                      "positive_words_sum_hash"

    # Default values
    raw = "../semeval2016_task4/100_topics_100_tweets.sentence-three-point" \
          ".subtask-A.train.gold.txt"
    tweets = "../semeval2016_task4/train/tweets.txt"
    test = "../semeval2016_task4/dev/tweets.txt"
    no_preprocessing = False
    alternative_bow = False
    use_pos_tags = True
    remove_hashtags = True
    remove_urls = True
    use_emoticons = True
    remove_handles = True
    remove_punctuation = True
    polarity_for_bow = True
    use_polarity = True
    remove_stopwords = True
    multiclass = "OneVsRest"
    # MNB
    if clf == "MNB":
        name = "MultiNominalNaiveBayes"
        learn_prior = False
        alpha = 1.0
    # LogReg
    if clf == "LogReg":
        name = "LogisticRegression"
        penalty = "l1"
        dual = False
        C = 0.1
        max_iter = 1000
        random_state = 42
        solver = "liblinear"
        tol = 0.001
        multi_class_l = "ovr"
    # SVM
    if clf == "SVM":
        name = "SVM"
        C_s = 10
        kernel = "linear"
        degree = 1
        gamma = 0.0
        coef0 = 0.0
        shrinking = True
        tol = 0.01
        cache_size = None
        max_iter = -1
        random_state = 42

    use_test_set = True
    build_full_model = True
    base = "alt_full_with_3_feature_spaces_43_features_cpd_1_0_"
    use = True
    use_full = True
    continue_fold = False
    use_test_set_p = False
    is_regression = False
    outer_folds = 10
    inner_folds = 3
    shuffle = True
    type_ = "k"
    train_size = 0.8
    roc = True
    train_size_s = 70
    grid_search = False
    use_f = False
    features_to_remove = "$$$_pos_emoticons,$$$_neg_emoticons,$$$_has_hashtag,$$$_neg_hash,$$$_obj_hash,$$$_pos_hash,$$$_negative_words_hash,$$$_objective_words_hash,$$$_positive_words_hash,$$$_polarity_words_hash,$$$_negation_words_hash,$$$_negative_words_sum_hash,$$$_objective_words_sum_hash,$$$_positive_words_sum_hash"
    dimensionality_reduction = None
    cpd_threshold = 0.0
    base_o = "../results"

    lines = OrderedDict()
    headers = ["[input]", "[preprocess]", "[output]", "[feature_selection]",
               "[classifier]", "[evaluation]"]
    lines["[input]"] = ""
    lines["raw"] = values.get("raw", raw)
    lines["tweets"] = values.get("tweets", tweets)
    lines["test"] = values.get("test", test)
    lines["[preprocess]"] = ""
    lines["bow"] = OrderedDict()
    lines["bow"]["min_df"] = values.get("bow", {}).get("min_df", min_df)
    lines["bow"]["max_df"] = values.get("bow", {}).get("max_df", max_df)
    lines["bow"]["stemmer_lang"] = values.get("bow", {}).get("stemmer_lang",
                                                             stemmer_lang)
    lines["bow"]["stemmer"] = values.get("bow", {}).get("stemmer", stemmer)
    lines["bow"]["use_bigrams"] = values.get("bow", {}).get("use_bigrams",
                                                            use_bigrams)
    lines["bow"]["more_attributes"] = values.get("bow", {}).get(
        "more_attributes", more_attributes)
    lines["bow"]["no_preprocessing"] = values.get("bow", {}).get(
        "no_preprocessing", no_preprocessing)
    lines["bow"]["alternative_bow"] = values.get("bow", {}).get(
        "alternative_bow", alternative_bow)
    lines["bow"]["use_pos_tags"] = values.get("bow", {}).get("use_pos_tags",
                                                             use_pos_tags)
    lines["bow"]["remove_hashtags"] = values.get("bow", {}).get(
        "remove_hashtags", remove_hashtags)
    lines["bow"]["remove_urls"] = values.get("bow", {}).get("remove_urls",
                                                            remove_urls)
    lines["bow"]["use_emoticons"] = values.get("bow", {}).get("use_emoticons",
                                                              use_emoticons)
    lines["bow"]["remove_handles"] = values.get("bow", {}).get(
        "remove_handles", remove_handles)
    lines["bow"]["remove_punctuation"] = values.get("bow", {}).get(
        "remove_punctuation", remove_punctuation)
    lines["bow"]["polarity_for_bow"] = values.get("bow", {}).get(
        "polarity_for_bow", polarity_for_bow)
    lines["bow"]["use_polarity"] = values.get("bow", {}).get("use_polarity",
                                                             use_polarity)
    lines["bow"]["remove_stopwords"] = values.get("bow", {}).get(
        "remove_stopwords", remove_stopwords)
    lines["ignore_invalid_tweets"] = values.get("ignore_invalid_tweets",
                                                ignore_invalid_tweets)
    lines["ignore_error_messages"] = values.get("ignore_error_messages",
                                                ignore_error_messages)
    lines["[classifier]"] = ""
    lines["multi_class"] = OrderedDict()
    lines["multi_class"]["type"] = values.get("multi_class", {}).get(
        "type", multiclass)
    lines["name"] = name
    if clf == "MNB":
        lines["learn_prior"] = values.get("learn_prior", learn_prior)
        lines["alpha"] = values.get("alpha", alpha)
    if clf == "LogReg":
        lines["penalty"] = values.get("penalty", penalty)
        lines["dual"] = values.get("dual", dual)
        lines["C"] = values.get("C", C)
        lines["max_iter"] = values.get("max_iter", max_iter)
        lines["random_state"] = values.get("random_state", random_state)
        lines["solver"] = values.get("solver", solver)
        lines["tol"] = values.get("tol", tol)
        lines["multi_class_l"] = values.get("multi_class_l", multi_class_l)
    if clf == "SVM":
        lines["C"] = values.get("C", C_s)
        lines["kernel"] = values.get("kernel", kernel)
        lines["degree"] = values.get("degree", degree)
        lines["gamma"] = values.get("gamma", gamma)
        lines["coef0"] = values.get("coef0", coef0)
        lines["shrinking"] = values.get("shrinking", shrinking)
        lines["tol"] = values.get("tol", tol)
        lines["cache_size"] = values.get("cache_size", cache_size)
        lines["max_iter"] = values.get("max_iter", max_iter)
        lines["random_state"] = values.get("random_state", random_state)
    lines["[evaluation]"] = ""
    lines["use_test_set"] = values.get("use_test_set", use_test_set)
    lines["build_full_model"] = values.get("build_full_model", build_full_model)
    lines["precomputed"] = OrderedDict()
    lines["precomputed"]["base"] = values.get("precomputed", {}).get(
        "base", base)
    lines["precomputed"]["use"] = values.get("precomputed", {}).get(
        "use", use)
    lines["precomputed"]["use_full"] = values.get("precomputed", {}).get(
        "use_full", use_full)
    lines["precomputed"]["continue_fold"] = values.get("precomputed", {}).get(
        "continue_fold", continue_fold)
    lines["precomputed"]["use_test_set"] = values.get("precomputed", {}).get(
        "use_test_set", use_test_set_p)
    lines["is_regression"] = values.get("is_regression", is_regression)
    lines["cv"] = OrderedDict()
    lines["cv"]["outer_folds"] = values.get("cv", {}).get("outer_folds",
                                                          outer_folds)
    lines["cv"]["inner_folds"] = values.get("cv", {}).get("inner_folds",
                                                          inner_folds)
    lines["cv"]["shuffle"] = values.get("cv", {}).get("shuffle", shuffle)
    lines["cv"]["type"] = values.get("cv", {}).get("type", type_)
    lines["cv"]["train_size"] = values.get("cv", {}).get("train_size",
                                                         train_size)
    lines["metrics"] = OrderedDict()
    lines["metrics"]["roc"] = values.get("metrics", {}).get("roc", roc)
    lines["split"] = OrderedDict()
    lines["split"]["train_size"] = values.get("split", {}).get("train_size",
                                                               train_size_s)
    lines["grid_search"] = values.get("grid_search", grid_search)
    lines["[feature_selection]"] = ""
    lines["use"] = values.get("use", use_f)
    lines["features_to_remove"] = values.get("features_to_remove",
                                              features_to_remove)
    lines["dimensionality_reduction"] = values.get(
        "dimensionality_reduction", dimensionality_reduction)
    lines["cpd_threshold"] = values.get("cpd_threshold", cpd_threshold)
    lines["[output]"] = ""
    lines["base"] = values.get("base", base_o)

    attrs = lines["bow"]["more_attributes"]

    # Define different feature groups for naming purposes
    emo = "emo_" if "pos_emoticons" in attrs else ""
    punc = "punc_" if "punctuations" in attrs else ""
    length = "len_" if "start_len" in attrs else ""
    perc = "perc_" if "adj_percentage" in attrs else ""
    tag = "tag_" if "has_hashtag" in attrs else ""
    al = "alche_" if "alchemy_tweet_polarity" in attrs else ""
    pol = "pol_" if "neg,obj,pos" in attrs else ""
    counts = "counts_" if "elongated" in attrs else ""
    hash_ = "hash_" if "neg_hash" in attrs else ""
    name = "features_" + emo + punc + length + perc + tag + al + pol + counts\
                             + hash_
    # Delete trailing "_"
    name = name[:-1] + ".ini"
    dst = os.path.join(out_dir, name)
    print "name", name
    # Write information to file
    with open(dst, "wb") as f:
        for name, vals in lines.iteritems():
            # print "Name", name
            # A header
            if name in headers:
                f.write(name + "\n")
            else:
                # Normal entry
                f.write(name + ": ")
                # Add indent to indicate suboptions with values
                if isinstance(vals, OrderedDict):
                    f.write("\n")
                    for k, v in vals.iteritems():
                        f.write("\t{}: {}\n".format(k, v))
                else:
                    f.write("{}\n".format(vals))


def get_difference(s1, s2):
    """
    Returns the difference of the 2 strings representing features. Each
    feature is separated by a comma.

    Parameters
    ----------
    s1: String separated by commas.
    s2: String separated by commas.

    Returns
    -------
    String representing the difference in the features of the tweets
    separated by commas.

    """
    s1 = s1.split(",")
    s2 = s2.split(",")
    # Subtract smaller set from larger one
    if len(s1) > len(s2):
        diff = set(s1) - set(s2)
    else:
        diff = set(s2) - set(s1)
    s = ""
    # Convert set to string
    for feature in diff:
        s += feature + ","
    return s[:-1]


def to_string(subsets):
    """
    Converts a list of subsets into a string that can be stored in a config
    file.

    Returns
    -------
    str.
    String representing all features to be used to represent tweets separated
    by commas.

    """
    features = ""
    for subset in subsets:
        for feature in subset:
            features += feature + ","
    # Skip trailing ","
    return features[:-1]

if __name__ == "__main__":
    out_dir = "configs/feature_subset_selection"
    all_attributes = "pos_emoticons,neg_emoticons,punctuations,start_len," \
                     "end_len,avg_len,adj_percentage,adv_percentage," \
                     "verb_percentage,noun_percentage,has_hashtag," \
                     "alchemy_tweet_polarity,alchemy_tweet_type," \
                     "alchemy_entity_polarity,alchemy_entity_type," \
                     "negative_entities,neutral_entities,positive_entities," \
                     "mixed_sentiment,neg,obj,pos,elongated,upper," \
                     "negative_words,objective_words,positive_words," \
                     "polarity_words,negation_words,negative_words_sum," \
                     "objective_words_sum,positive_words_sum,neg_hash," \
                     "obj_hash,pos_hash,negative_words_hash," \
                     "objective_words_hash,positive_words_hash," \
                     "polarity_words_hash,negation_words_hash," \
                     "negative_words_sum_hash,objective_words_sum_hash," \
                     "positive_words_sum_hash"
    a = all_attributes.split(",")
    print "#features", len(a)
    print a
    subsets = [
        # Emoticons
        ["pos_emoticons", "neg_emoticons"],
        # Punctuation
        ["punctuations", "start_len", "end_len", "avg_len"],
        # Percentages
        ["adj_percentage", "adv_percentage", "verb_percentage",
         "noun_percentage"],
        # Hashtag
        ["has_hashtag"],
        # Alchemy
        ["alchemy_tweet_polarity", "alchemy_tweet_type",
         "alchemy_entity_polarity", "alchemy_entity_type",
         "negative_entities", "neutral_entities",
         "positive_entities", "mixed_sentiment"],
        # Polarity
        ["neg", "obj", "pos"],
        # Counts
        ["elongated", "upper", "negative_words",
         "objective_words", "positive_words", "polarity_words",
         "negation_words", "negative_words_sum",
         "objective_words_sum", "positive_words_sum"],
        # Hashtag polarity
        ["neg_hash", "obj_hash", "pos_hash", "negative_words_hash",
         "objective_words_hash", "positive_words_hash",
         "polarity_words_hash", "negation_words_hash",
         "negative_words_sum_hash", "objective_words_sum_hash",
         "positive_words_sum_hash"]
    ]
    print "subsets", len(subsets)
    print "combinations", len(list(itertools.combinations(subsets,
                                                          len(subsets) - 1)))
    # Remove a single subset
    # for combi in itertools.combinations(subsets, len(subsets) - 1):
    #     print combi
    #     print to_string(combi)
    # Remove all but one subset
    for combi in itertools.combinations(subsets, 2):
        print combi
        print "-----"
        features_subset = to_string(combi)
        print "subset to be used:", len(features_subset.split(",")), \
            features_subset
        to_remove = get_difference(all_attributes, features_subset)
        print "to be removed:", len(to_remove.split(",")), to_remove
        print "total attributes (should be 43)", (len(to_remove.split(",")) +
                                                  len(features_subset.split(",")))
        print create_config({"features_to_remove": to_remove, "bow": {
            "more_attributes": features_subset}}, out_dir, clf="SVM")
    total = 0
    # For each potential subset of length i
    # for i in xrange(1, len(subsets) + 1):
    #     total += len(list(itertools.combinations(subsets, i)))
    #     for combi in itertools.combinations(subsets, i):
    #         print combi
    print "Total combinations", total
    # create_config({"metrics": {"roc": "False"}}, out_dir, clf="SVM")
